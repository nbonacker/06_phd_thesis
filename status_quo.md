
# Status quo: Network inference and response porangeiction in biological system

## Foreword

<span style="color:orange">to do: write down</span>

### Gene regulatory network inference

### Cancer Immunotherapy response prediction

## Foundations

### Theoretical foundations

### Biological foundations

## Gene regulatory network inference

<span style="color:green">revised</span>

### Motivation

<span style="color:green">revised</span>

### Review

<span style="color:green">revised</span>

### Stochastic modelof gene regulation

<span style="color:green">revised</span>

### Forward problem

<span style="color:green">revised</span>

### Inverse problem

<span style="color:green">revised</span>
idea: inference with prior in appendix
idea: inference with parameter optimization in appendix

### Inference and response porangeiction in a melanoma cell line

<span style="color:orange">to do: discuss results</span>
<span style="color:red">to do: response prediction</span>

## Cancer immunotherapy response predictiond

<span style="color:orange">to do: revise</span>

### Cancer immunotherapy by immune checkpoint blockade

<span style="color:orange">to do: finish writing</span>

### Foundations of response prediction and survival analysis

<span style="color:orange">to do: revise</span>

### Response prediction

<span style="color:orange">to do: write down</span>
<span style="color:red">add numper of binding peptides as a predictor</span>
<span style="color:red">include p-value p=0.05 in regression coefficent analysis</span>

### Survival analysis

<span style="color:orange">to do: write down</span>
<span style="color:red">include p-value p=0.05 in regression coefficent analysis</span>

### Conclusion about the immunogenetic potential of frameshift mutation

## Epilogue

<span style="color:orange">to do: write down</span>
