
\chapter{Gene regulatory network inference} \label{chapter:Gene regulatory network inference}

\epigraph{If people do not believe that mathematics is simple, it is only because they do not realise how complicated life is.}{John von Neumann}

The problem of gene regulatory network inference (GRNI) is the primary objective of this chapter.
We address the question of whether one can learn something out of correlations between gene expression measurements for the reconstruction of regulatory interactions.
Therefore we develop a maximum likelihood method (MLM), and we compare the performance of our MLM to least squares fits, which are standard methods for GRNI \cite{ Nelander2008, Korkut2015}.\\

In section \ref{section:Motivation to gene regulatory network inference}, we highlight research areas where knowledge of GRN is essential.
We motivate the reconstruction of small signalling cascades to large gene regulatory networks consisting of thousands of genes.

The revealing of unknown regulatory relationships is based on experimental protocols to quantify gene expression, a mathematical model of the gene regulatory network (GRN), and the inference of model parameters given gene expression data.
In section \ref{section:Approaches to gene regulatory network inference} the state-of-art of experimental protocols and mathematical approaches to GRNI is reviewed.

Gene expression is an intrinsic stochastic process. 
We try to learn something from correlations between fluctuations in gene expression about the regulatory interactions.
Therefore, we propose a stochastic model of gene regulation.
We model gene regulation dynamics by a system of stochastic differential equations.
Our stochastic model of gene regulation, which we define in section \ref{section: Stochastic model of gene regulation}, is based on a deterministic model developed by Nelander \cite{Nelander2008}.

We divide the complex challenge of GRNI up into two problems.
The first problem is the forward problem, the calculation of mean and covariance of gene expression given model parameters.
The second problem is the inverse problem, the inference of model parameters given gene expression data.
An accurate solution to the forward problem is crucial for our inference approach, as our MLM depends to a large extent on the forward problem of our stochastic model.
Therefore, we focus in the first step on a precise solution to the forward problem.

To solve the forward problem, we employ a Gaussian theory (GT) developed by Mézard and Sakallariou in the context of the asymmetric Ising model \cite{Mezard2011} in section \ref{section:Forward problem}.
We compute mean gene expression levels and their correlations in the steady state.
The GT results are compared to the results of mean field theory (MFT) and a numeric solution of our stochastic model.
We show that the GT outperforms the mean field approach in the regime of strong gene interaction.
We expected this behaviour because our MFT is based on an expansion in the interactions around a factorising model.
In contrast, the GT is independent of the coupling strength.

We focus on inverse problems where time-series data is not available, which is typical for high-throughput experiments in the context of GRNI.
To solve the inverse problem, we apply a maximum likelihood method (MLM) in section \ref{section:Inverse problem}.
Our MLM is based on the GT for the solution of the forward problem.
We compare the results of our MLM to standard inference approaches based on least squares fits of the first moments of the steady state distribution \cite{ Nelander2008, Korkut2015}.
We demonstrate that our MLM outperforms least squares methods in the regime of a significant contribution of stochastic noise to the system dynamics.

Finally, we perform an inference and response prediction of a small GRN in a melanoma cell line based on experimental data in section \ref{section:Inference and response prediction in a melanoma cell line}.
Our findings give evidence that our MLM results in a more precise response prediction than least squares methods.

\input{chapter/network_inference/sections/motivation_to_gene_regulatory_network_inference.tex}

\input{chapter/network_inference/sections/approaches_to_gene_regulatory_network_inference.tex}

\input{chapter/network_inference/sections/stochastic_model_of_gene_regulation.tex}

\input{chapter/network_inference/sections/forward_problem.tex}

\input{chapter/network_inference/sections/inverse_problem.tex}

\input{chapter/network_inference/sections/network_inference_and_response_prediction_in_melanoma_cell_line.tex}
