
\subsection{Mean field theory} \label{subsection: Mean field theory}

The general idea of MFT is to compute high-dimensional sums or integrals over random variables under the assumption that one can neglect dependencies between the variables.
An excellent and comprehensive description of MFT can be found in the textbook by Opper and Saad \cite{OpperSaad2001}, which covers the foundations of different approaches to MFT and demonstrates their application to various areas of probabilistic modelling.
The textbook by Mézard, Parisi and Virasoro \cite{MezardParisiVirasoro1986} contains a comprehensive and self-contained presentation of spin glass theory.
The theory of spin glasses demonstrates a rich behaviour.
Methods to analyse fluctuations around the mean field solution provide insights into other complex systems, such as our stochastic model of gene expression.\\

In the first paragraph of this subsection, we outline the general idea of MFT based on the free energy function.
In the second paragraph, a variational approach to MF is introduced.
This variational approach is even applicable in the absence of a free energy function, which is the case in our stochastic model for gene regulation.
We solve the forward problem of our stochastic model of gene expression within a mean field approximation in the closing paragraph.
To this end, we employ an approach based on an idea by Kappen and Spanjers in the context of asymmetric neural networks \cite{Kappen2000}.

In the second paragraph, a variational approach to MF is introduced.
This variational approach is even applicable in the absence of a free energy function, which is the case in our stochastic model for gene regulation.

\subsubsection{General idea of MFT}

The general idea of MFT is to approximate a large number of additive contributions to a system hamiltonian by a mean field.
For this purpose one studies a system with a Hamiltonian,
\begin{align}
\H = \H^0 + \H^\text{int} \, \text{,}
\end{align}
composed of an interacting part, $\H^\text{int}$, and a non-interacting part, $\H^0$.
One assumes that the system, which one tries to approximate, contains only pairwise interactions,
\begin{align}
\H^\text{int} = \sum_\text{ij} h_\text{ij}( x_\text{i}, x_\text{j}) \, \text{.}
\end{align}
Within such a bipartite system the Bogoliubov inequality states that the free energy of the whole system, $F$, is bounded from above,
\begin{align}
F \leq F^0 := \left< \H \right>_0 - TS^0 \, \text{,}
\end{align}
by the free energy of the non-interacting system, $ F^0 = -\kB T \ln Z^0$  \cite{Callen1985}.
The statistical properties of the non-interacting system are described by the partition function, $Z^0 = \sum_ \mathbf{x} \e^{ -β\H^0( \mathbf{x})}$. 
The non-interacting free energy, $ F^0$, is used as an approximation from above for the free energy of the entire system, $F$.
To calculate $\left< \H \right>_0$ and the entropy $S^0$ one employs the normalised Bolzmann distribution,
\begin{align}
p^0( \mathbf{x}) = \frac{ \e^{ - β \H^0( \mathbf{x})}}{Z^0} = \prod_\text{i} \underbrace{ \frac{ \e^{ -β h_\text{i}( x_i)}}{Z^0_\text{i}}}_{ =: p^0_\text{i}( x_\text{i})} \, \text{,}
\end{align}
of the non-interacting system.
The minimisation of $F_0$ with respect to the non-interacting distribution, $p^0_\text{i}( x_\text{i})$, results in a set of self-consistent equations,
\begin{align}
p^0_\text{i}( x_\text{i}) = \frac{ \e^{ - β h_\text{i}^\text{MF}( x_\text{i})}}{Z_0} \, \text{,}
\end{align}
in which the pairwise interactions are approximated by the field,
\begin{align}
h_\text{i}^\text{MF}( x_\text{i}) = \sum_\text{j} \int h_\text{ij}( x_\text{i}, x_\text{j}) p^0_\text{j}( x_\text{j}) \d x_j \, \text{.}
\end{align}
This field, $h_\text{i}^\text{MF}( x_\text{i})$, is referred to as a MF because it incorporates the time averaged pairwise interaction energy between the system and the degree of freedom $x_\text{i}$. 

\subsubsection{Variational approach to MFT}

We can not construct a free energy function within our stochastic model of gene regulation because there is no such Hamiltonian, $\H$.
On account of the absence of a free energy function, the general idea of MFT, outlined in the paragraph above, is not applicable within our model.
Instead of minimising the free energy, one employs a variational approach based on the distribution of system states in this setting.

The system is characterised with a multivariate probability distribution, $p( \mathbf{x})$, and one minimises the Kullback–Leibler divergence,
\begin{align}
\DKL \left( p( \mathbf{x})| q( \mathbf{x}) \right) = \sum_ \mathbf{x} q(\mathbf{x}) \frac{ q( \mathbf{x})}{ p( \mathbf{x})} = \left< \ln  \frac{ q( \mathbf{x})}{ p( \mathbf{x})} \right>_{q} \, \text{,}
\end{align}
between $p( \mathbf{x})$ and a variational distribution, $q(  \mathbf{x})$.
The probability distribution, $p( \mathbf{x})$, is characterised by the Boltzmann distribution,
\begin{align}
p( \mathbf{x}) = \frac{\e^{ -\H( \mathbf{x})}}{ Z} \, \text{,}
\end{align}
with the partition function, $Z = \sum_ \mathbf{x} \e^{ -\H( \mathbf{x})}$, as a normalisation constant.
Thus, the Kullback–Leibler divergence,
\begin{align} \label{eqn:variational_free_energy}
\DKL \left( p( \mathbf{x})| q( \mathbf{x}) \right) = \ln Z + \underbrace{ E[q] - S[q]}_{ =: F[q ]} \, \text{,}
\end{align}
can be expressed in terms of the variational energy, $E[q] = \sum_\mathbf{x} q( \mathbf{x}) \H( \mathbf{x})$, and the entropy, $S[q] = - \sum_\mathbf{x} q( \mathbf{x}) \ln q( \mathbf{x})$.
Such that a minimal Kullback–Leibler divergence, $\DKL$, corresponds to a minimum in the variational free energy, $F[ q]$.
The space of distributions is restricted to factorising distributions,
\begin{align}
q( \mathbf{x}) = \prod_\text{i} q_\text{i}(x_\text{i}) \, \text{,}
\end{align}
accordingly to the general idea of MFT to approximate the system interaction by a meanfield.\\

Within the MFT of our stochastic model of gene regulation in the following paragraph, we require the factorising distribution, $ q( \mathbf{x})$, to retain the mean expectation value, $\left< x_\text{i} \right>_p = \left< x_\text{i} \right>_q$.
This requirement is motivated by the Ising model, where the minimal Kullback–Leibler divergence corresponds to retaining mean expectation values.

To find this result we study the Ising model with binary spin variables, $ x_\text{i} \in \left\{ -1, 1 \right\}$, as introduced in subsection \eqref{subsection:Connection to the asymmetric Ising model}.
The Hamiltonian is given by
\begin{align}
\H( \mathbf{x}) = - \sum_\text{ij} J_\text{ij} x_\text{i} x_\text{j} - \sum_\text{i} x_\text{i} θ_\text{i} \, \text{.}
\end{align}
For the factorising distribution, one sets the couplings equal to zero, $J_\text{ij}^{*}=0$, and variates the external fields, $θ_\text{i}^{*}$, to minimise $\DKL$.
The most general form of a factorizing distribution is a product of Bernoulli distributions,
\begin{align}
q( \mathbf{x}) = \prod_\text{i} \left( \dfrac{ 1 + m_\text{i}^{*}( θ_\text{i}^{*}) x_\text{i}}{2} \right) \, \text{,} 
\end{align} % = \prod_\text{i} \left( \frac{ 1 - x_\text{i}}{2} p^{-}_\text{i} + \frac{ 1 + x_\text{i}}{2} p^{+}_\text{i} \right)
where $ m_\text{i}^{*}( θ_\text{i}^{*})$ is the expectation value of variable $x_\text{i}$.
Setting the derivative of the Kullback–Leibler divergence with respect to the 
$θ_\text{i}^{*}$ equal to zero,
\begin{align}
\begin{split}
0 &\stackrel{!}{=} \dfrac{ \partial}{ \partial θ_\text{i}^{*}} \sum_{ \mathbf{x}} p( \mathbf{x}) \sum_\text{k} \ln \left( \dfrac{ 1 + m_\text{k}^{*}( θ_\text{k}^{*})x_\text{k}}{2} \right) \\
&= \left( \dfrac{ p( x_\text{i} = +1)}{ 1 + m_\text{i}^{*}( θ_\text{i}^{*})} -  \dfrac{ p( x_\text{i} = -1)}{ 1 - m_\text{i}^{*}( θ_\text{i}^{*})}  \right) \left( 1 - {m_\text{i}^{*}}^{2}( θ_\text{i}^{*})\right) \\
&= \left< x_\text{i} \right>_\text{s} - m_\text{i}^{*}( θ_\text{i}^{*})
\end{split}
\end{align}
we find the requirement of retaining the mean expectation values.

\iffalse
Moreover a set of self consistent mean field equations is derived,
\begin{align} \label{eqn:mean_field _approximation_ising_model}
m_\text{i} = \tanh \left(  \sum_{ \text{k}} ω_{ \text{ik}} m_\text{k} + θ_\text{k} \right) \, \text{.}
\end{align}
\fi

\subsubsection{MFT of our stochastic model of gene regulation}

In this paragraph, we employ MFT to solve the forward problem of our stochastic model of gene regulation.
The approach is based on an idea developed by Kappen and Spanjers in the context of asymmetric neural networks \cite{Kappen2000}.
Kappen and Spanjers derived self-consistent equations up to the second order in the couplings to calculate mean firing rates and their correlations.

The neural network used by Kappen and Spanjers in their mean field calculation is an asymmetric Ising model equipped with sequential Glauber dynamics.
The Ising model is in contrast to our stochastic model of gene regulation discrete.
Nevertheless, the steady state averages for the first two moments, $\left< x_\text{i}^\mu \right>_\text{s}$ and $\left< x_\text{i}^\mu x_\text{j}^\mu \right>_\text{s}$, are similar as we showed in subsection \ref{subsection:Connection to the asymmetric Ising model}.

We follow the arguments by Kappen and Spanjers.
The general idea is to approximate the intractable distribution of gene expression level, $p( \mathbf{x}^μ)$, with an optimal factorising distribution, $q^*( \mathbf{x}^μ)$.
Further we expand mean gene expression, $m_\text{i}^μ$, and covariance of gene expression, $χ_\text{ij}^μ$, up to second order in the interaction matrix, $ω$, around $q^*( \mathbf{x}^μ)$.
Within our stochastic model of gene regulation we derive a system of self-consistent equations to calculate $m_\text{i}^μ$ and $χ_\text{ij}^μ$.\\

The calculation is based on the steady state averages defined in equation \eqref{eqn:exact_relations}.
We set the model parameter $a_\text{i}, b_\text{i}, \text{ and } c_\text{i}$ equal to one and substitute the perturbation, $u^μ_\text{i}$, into a perturbation dependent expression threshold, $θ^μ_\text{i} = θ_\text{i} + u^μ_\text{i}$.
The mean and covariance,
\begin{align} \label{eqn:basis_mean_field_theory}
\begin{split}
m_\text{i}^μ &\stackrel{\eqref{eqn:exact_relations}}{=} \left< \tanh \left( h_\text{i}^μ \right) \right>_\text{s} \quad \text{with } h_\text{i}^μ(  \mathbf{x}^μ ) = \sum_{ \text{k}} ω_{ \text{ik}} x_\text{k}^μ + θ^μ_\text{i} \\
χ_\text{ij}^μ &\stackrel{\eqref{eqn:exact_relations}}{=} \frac{1}{2} \left< \left( x_\text{i}^μ - m_\text{i}^μ \right) ( \tanh \left( h_\text{j}^μ \right) - m_\text{j}^μ) \right>_\text{s} + \left( \text{i} \leftrightarrow \text{j} \right) + \frac{1}{2} \delta_\text{ij} \, \text{,}
\end{split}
\end{align}
are expanded around the factorising probability distribution, $q^*( \mathbf{x}^μ | θ^{*μ})$.
The construction of $q^*( \mathbf{x}^μ | θ^{*μ})$ is based on non-interacting gene expression, $ω_\text{ij}^{*μ} = 0$, and a mean field, $θ^{*μ}_\text{i}$.
We require to recover the mean gene expression of the full gene regulatory model within the factorising distribution,
\begin{align}  \label{eqn:mft_condition_on_theta}
% \left< x_\text{i}^{ μ} \right>_{q} =: m_\text{i}^{ μ q} \stackrel{!}{=} m_\text{i}^{ μ q*} := \left< x_\text{i}^{ μ} \right>_{q^*} = \tanh \left( θ_\text{i}^{ μ q^*} \right) \label{eqn:mft_m}
\left< x_\text{i}^{ μ} \right>_{q} \stackrel{!}{=} \left< x_\text{i}^{ μ} \right>_{q^*} = \tanh \left( θ_\text{i}^{ *μ} \right) \, \text{.}
\end{align}
Such that the external field, $h_\text{i}^μ(  \mathbf{x}^μ ) $, is approximated by a mean field, $θ^{* μ}_\text{i}$.
The expansion of $m_\text{i}^μ$ and $χ_\text{ij}^μ$ at $q^*( \mathbf{x}^μ | θ^{*μ})$ is performed in the variables $δω_\text{ij}$ and $δθ_\text{i}^{ μ}$, which are defined as
\begin{align} \label{eqn:mft_variables}
\begin{split}
ω_\text{ij} &= 0 + δω_\text{ij} \\
θ_\text{i}^{ μ} &= θ_\text{i}^{ *μ} + δθ_\text{i}^{μ} \, \text{.}
\end{split}
\end{align}
The Taylor expansion of the mean gene expression, $m^{μ}_\text{i}$, up to quadratic order in $δω_\text{ij}$ and $δθ_\text{i}^{ μ}$ around the factorising distribution,
\begin{align} \label{eqn:mft_taylor_expansion}
\begin{split}
m^{μ}_\text{i}\left( \delta θ, \delta ω \right) = m^{*μ}_\text{i}
&+ \sum _\text{j} \left. \dfrac { \partial m^{ μ}_\text{i}}{ \partial θ_\text{j}} \right| _{ q^{ \ast}}  \delta θ _\text{j} + \sum _\text{jk} \left. \dfrac { \partial m^{ μ}_\text{i}}{ \partial ω_\text{ jk}} \right| _{ q^{ \ast}}  \delta ω _\text{jk} \\
&+ \dfrac {1}{2} \sum _\text{ jk} \left. \dfrac { \partial^2 m^{ μ}_\text{i}}{ \partial θ_\text{ j} \partial θ_\text{ k}} \right| _{ q^{ \ast}}  \delta θ _\text{j} \delta θ _\text{ k} \\
&+ \sum _\text{jkl} \left. \dfrac { \partial^2 m^{ μ}_\text{i}}{ \partial θ_\text{j} \partial ω_\text{kl}} \right| _{ q^{ \ast}}  \delta θ _\text{j} \delta ω _\text{kl} \\
&+ \frac{1}{2} \sum _\text{ jklm} \left. \dfrac { \partial^2 m^{ μ}_\text{i}}{ \partial ω_\text{jk} \partial ω_\text{lm}} \right| _{ q^{ \ast}}  \delta ω _\text{jk} \delta ω _\text{lm} + \sigma \left( \delta^3 \right) \, \text{,}
\end{split}
\end{align}
is the basis of our mean field calculation.
To calculate first order terms in the Taylor expansion of $ m^{ μ}_\text{i} = \int \d x \left[ p \left( \mathbf{x} | θ ω \right) \tanh \left( h^{ μ}_\text{i} \right) \right]$ one employs partial differentiation at $q*$,
\begin{align} \label{eqn:tanh_h_q*}
\left. \tanh \left( h^{ μ}_\text{i} \right) \right|_{ q^{ \ast}} = m^{ *μ}_\text{i}
\end{align}
\begin{align}
\left. \dfrac{ \partial \tanh \left( h^{ μ}_\text{i} \right)}{ \partial θ_\text{ j}} \right|_{ q^{ \ast}} = \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta_\text{ij} \label{eqn:tanh_h_partial_theta_q*}
\end{align}
\begin{align}
\left. \dfrac{ \partial \tanh \left( h^{ μ}_\text{i} \right)}{ \partial ω_\text{jk}} \right|_{ q^{ \ast}} = \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta_\text{ij} x^{ μ}_\text{k} \label{eqn:tanh_h_partial_omega_q*} \, \text{.}
\end{align}
With these partial derivatives and integration over gene expression levels, $\mathbf{x}$, we obtain first order Taylor coefficients,
\begin{align}
\begin{split}
\left. \dfrac{ \partial m^{ μ}_\text{i}}{ \partial θ_\text{j}} \right|_{ q^{ \ast}} &= \int \d \mathbf{x} \left[ \dfrac { \partial p \left( \mathbf{x} | θ ω \right)}{ \partial θ _\text{j}} \underbrace{ \tanh \left( h^{ μ}_\text{i} \right)}_{ \stackrel{ \eqref{eqn:tanh_h_q*}}{=} m^{ μ q}_\text{i}} \right]_{ q^{ \ast}} \\
& \qquad + \int \d \mathbf{x} \left[ p \left( \mathbf{x} | θ ω \right) \underbrace{ \dfrac{ \partial \tanh \left( h^{ μ}_\text{i} \right)}{ \partial θ_\text{j}}}_{ \stackrel{ \eqref{eqn:tanh_h_partial_theta_q*}}{=} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta_\text{ij}} \right]_{ q^{ \ast}} \\
&= 0 + \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta_\text{ij} \quad \text{and}
\end{split}
\end{align}
\begin{align} \label{eqn:m_partial_omega_q*}
\begin{split}
\left. \dfrac{ \partial m^{ μ}_\text{i}}{ \partial ω_\text{jk}} \right| _{ q^{ \ast}} &= \int \d \mathbf{x} \left[ \dfrac { \partial p \left( \mathbf{x} | θ ω \right)}{ \partial ω_\text{jk}} \tanh \left( h^{ μ}_\text{i} \right)\right]_{ q^{ \ast}}\\
& \qquad + \int \d \mathbf{x} \left[ p \left( \mathbf{x} | θ ω \right) \underbrace{ \dfrac{ \partial \tanh \left( h^{ μ}_\text{i} \right)}{ \partial ω_\text{jk}}}_{ \stackrel{ \eqref{eqn:tanh_h_partial_omega_q*}}{=} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta_\text{ij} x^{ μ}_\text{k}} \right]_{ q^{ \ast}} \\
&= 0 + \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) m^{ μ}_\text{k} \delta_\text{ij}  \, \text{.}
\end{split}
\end{align}
Summing over all contributions linear in $δω$ and $δθ^{ μ}$ one obtains first order corrections to mean gene expression,
\begin{align} \label{eqn:theta_m_q*}
{}^{ θ} m^{ μ q^{ \ast}}_\text{i} := \sum _\text{j} \left. \dfrac { \partial m^{ μ}_\text{i}}{ \partial θ_\text{j}} \right| _{ q^{ \ast}}  \delta θ _\text{j} = \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta θ _\text{i} \quad \text{and}
\end{align}
\begin{align} \label{eqn:omega_m_q*}
{}^{ ω} m^{ μ q^{ \ast}}_\text{i} := \sum _\text{jk} \left. \dfrac { \partial m^{ μ}_\text{i}}{ \partial ω_\text{jk}} \right| _{ q^{ \ast}}  \delta θ _\text{j} = \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \sum_{k} \delta ω _\text{ik} m^{ μ}_\text{k} \, \text{.}
\end{align}
Based on the condition to recover mean gene expression of the full regulatory system in equation \eqref{eqn:mft_condition_on_theta} one gets a condition,
\begin{align} \label{eqn:condition_theta_m_omega_m}
0 \stackrel{!}{=} {}^{ θ} m^{ μ q^{ \ast}}_\text{i} + {}^{ ω} m^{ μ q^{\ast }}_\text{i} + \sigma \left( \delta^2 \right) \, \text{,}
\end{align}
on ${}^{ θ} m^{ μ q^{ \ast}}_\text{i}$ and ${}^{ ω} m^{ μ q^{ \ast}}_\text{i}$.
Using this condition and the results \eqref{eqn:theta_m_q*} and \eqref{eqn:omega_m_q*} one can calculate an analytic expression for $\delta θ$ in first order mean field approximation,
\begin{align}
\delta θ _\text{i}^\text{MFT1}= - \sum_{k} \delta ω _\text{ik} m^{ μ}_\text{k} \, \text{.}
\end{align}
With the analytic expression for $\delta θ _\text{i}^\text{MFT1}$ we finally calculate mean  gene expression for our stochastic model in first order MFT,
\begin{align} \label{eqn:m_mft1}
m^{ μ \, \text{MFT1}}_\text{i}= \tanh \left( \sum_{k} ω _\text{ik} m^{ μ}_\text{k} + θ_\text{i} \right) \, \text{,}
\end{align}
using the relation \eqref{eqn:mft_condition_on_theta} and definition of $\delta θ _\text{i}$ in equation \eqref{eqn:mft_variables}.
The mean field approximation for the mean gene expression in first order, $m^{ μ \, \text{MFT1}}_\text{i}$, turns out to be equivalent to the mean field approximation of the Ising model.

In the Appendix section \ref{appendix subsection: Mean gene expression in second order MFT} we calculate corrections in second order by evaluation of partial derivatives at the factorising distribution $q^*$ and integration over gene expression, $\mathbf{x}$.
The calculation is conceptually analogue to the first order calculation.

We find the repetitive pattern that in the calculation of higher order corrections one can identify lower order derivertives (such as $\left. \nicefrac{ \partial m^{ μ}_\text{n}}{ \partial ω_\text{jk}} \right| _{ q^{ \ast}}$ and $\left. \nicefrac{ \partial m^{ μ}_\text{n}}{ \partial θ_\text{i}} \right| _{ q^{ \ast}}$), which we have already calculated in first order MFT.
Summing over all contributions quadratic in $δω$ and $δθ^{ μ}$ one gets second order corrections in the mean gene expression,
\begin{align} \label{eqn:theta_theta_m_q*}
{}^{ θ θ} m^{ μ q^{ \ast}}_\text{i} := \sum _\text{jk} \left. \dfrac { \partial^2 m^{ μ}_\text{i}}{ \partial θ_\text{j} \partial θ_\text{k}} \right| _{ q^{ \ast}}  \delta θ _\text{j} \delta θ _\text{k} = ( -2) m^{ μ q}_\text{i} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \delta θ _\text{i}^2
\end{align}
\begin{align} \label{eqn:theta_theta_omega_m_q*}
\begin{split}
{}^{ θ ω} m^{ μ q^{ \ast}}_\text{i} &:= \sum _\text{jkl} \left. \dfrac { \partial m^{ μ}_\text{i}}{ \partial θ_\text{j} \partial θ_ω{kl}} \right| _{ q^{ \ast}}  \delta θ _\text{j} \delta ω _\text{kl} \\ &= \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \left( \vphantom{ \sum _\text{l}} ( -2) m^{ μ q}_\text{i} \delta θ _\text{i} \left( - \delta θ _\text{i}^\text{MFT1}\right) \right. \\ & \quad + \left. \sum _\text{l} \left(  1 - \left( m^{ μ q}_\text{l} \right)^2 \right) \delta ω _\text{il} \delta θ _\text{l} \right)
\end{split}
\end{align}
\begin{align} \label{eqn:omega_omega_m_q*}
\begin{split}
{}^{ ω ω} m^{ μ q^{ \ast}}_\text{i} &:= \sum _\text{jklm} \left. \dfrac { \partial^2 m^{ μ}_\text{i}}{ \partial ω_\text{jk} \partial ω_\text{lm}} \right| _{ q^{ \ast}}  \delta ω_\text{jk} \delta ω _\text{lm} \\
&= \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \left( \vphantom{ \sum _\text{m}} ( -2) m^{ μ q}_\text{i} \left(-θ _\text{i}^\text{MFT1} \right)^2 \right. \\ & \qquad + \left( -2 \right) m^{ μ q}_\text{i} \dfrac{1}{2} \sum_{j} \left(δω_\text{ij} \right)^2 \\
& \qquad + \left. 2 \sum_\text{m} δω_\text{im} \left(  1 - \left( m^{ μ q}_\text{m} \right)^2 \right)  \left(- δθ_\text{m}^\text{MFT1} \right) \right) \, \text{.}
\end{split}
\end{align}
Based on the condition to recover the mean gene expression also in second order an equation for the corrections up to quadratic order,
\begin{align}
\begin{split}
0 &\stackrel{!}{=} {}^{ θ} m^{ μ q^{ \ast}}_\text{i} + {}^{ ω} m^{ μ q^{\ast }}_\text{i} + \frac{1}{2} {}^{θθ} m^{ μ q^{ \ast}}_\text{i} + {}^{θω} m^{ μ q^{\ast }}_\text{i} + \dfrac{1}{2} {}^{ωω} m^{ μ q^{\ast }}_\text{i} + \sigma \left( \delta^3 \right) \, \text{,}
\end{split}
\end{align}
is derived.
Using second order corrections in equation \eqref{eqn:theta_theta_m_q*}, \eqref{eqn:theta_theta_omega_m_q*}, and \eqref{eqn:omega_omega_m_q*}, and the previous results \eqref{eqn:theta_m_q*} and \eqref{eqn:omega_m_q*}, an analytical expression for $\delta θ$ in second order mean field approximation,
\begin{align}
\delta θ _\text{i}^\text{MFT2} = \delta θ _\text{i}^\text{MFT1} + \dfrac{1}{2} m^{ μ}_\text{i} \sum_{k} \left( δω_\text{ik} \right)^2 \, \text{,}
\end{align}
is derived.
Analogue to the first order approximation we finally calculate the mean gene expression of our stochastic model in second order MFT,
\begin{align} \label{eqn:m_mft2}
m^{ μ \, \text{MFT2}}_\text{i} = \tanh \left( \sum_{k} ω _\text{ik} m^{ μ}_\text{k} + θ_\text{i} - \dfrac{1}{2} m^{ μ}_\text{i} \sum_{k} \left( ω _\text{ik} \right)^2 \right) \, \text{,}
\end{align}
using the equations \eqref{eqn:mft_condition_on_theta} and \eqref{eqn:mft_variables}.
The basis to MFT approximation of the covariance is the Taylor expansion of $χ^{μ}_\text{ij}$ around the factorising distribution, $q^*$,
\begin{align} \label{eqn:mft_taylor_expansion_x}
\begin{split}
χ^{μ}_\text{ij}\left( \delta θ, \delta ω \right) = χ^{ μ q^{ \ast}}_\text{i}
&+ \sum _\text{k} \left. \dfrac { \partial χ^{μ}_\text{ij}}{ \partial θ_\text{k}} \right| _{ q^{ \ast}}  \delta θ _\text{k} + \sum _\text{ kl} \left. \dfrac { \partial χ^{μ}_\text{ij}}{ \partial ω_\text{kl}} \right| _{ q^{ \ast}}  \delta ω _\text{kl} \\
&+ \dfrac {1}{2} \sum _\text{ jk} \left. \dfrac { \partial^2 χ^{μ}_\text{ij}}{ \partial θ_\text{k} \partial θ_\text{l}} \right| _{ q^{ \ast}}  \delta θ _\text{ k} \delta θ _\text{ l} \\
&+ \sum _\text{ klm} \left. \dfrac { \partial^2 χ^{μ}_\text{ij}}{ \partial θ_\text{ k} \partial ω_\text{ lm}} \right| _{ q^{ \ast}}  \delta θ _\text{k} \delta ω _\text{lm} \\
&+ \frac{1}{2} \sum _\text{klmn} \left. \dfrac { \partial^2 χ^{μ}_\text{ij}}{ \partial ω_\text{ kl} \partial ω_\text{ mn}} \right| _{ q^{ \ast}}  \delta ω _\text{kl} \delta ω _\text{mn} + \sigma \left( \delta^2 \right) \, \text{.}
\end{split}
\end{align}
To expand the term $\frac{1}{2} \int \d \mathbf{x} \left[  p \left( \mathbf{x} | θ ω \right) \left( x_\text{i}^μ - m_\text{i}^μ \right) ( \tanh \left( h_\text{j}^μ \right) - m_\text{j}^μ) \right]$ of the covariance  \eqref{eqn:basis_mean_field_theory} partial derivatives at $q^*$,
\begin{align} \label{eqn:tanh_m_patial_theta_q*}
\left. \dfrac{ \partial \left( \tanh \left( h^{ μ}_\text{i} \right) - m^{ μ}_\text{i} \right)}{ \partial θ_\text{k}}  \right|_{q*}  \stackrel{ \eqref{eqn:tanh_h_partial_omega_q*} \eqref{eqn:m_partial_omega_q*} }{=}0
\end{align}
\begin{align} \label{eqn:tanh_m_patial_omega_q*}
\left. \dfrac{ \partial \left( \tanh \left( h^{ μ}_\text{i} \right) - m^{ μ}_\text{i} \right)}{ \partial ω_\text{kl}} \right|_{q*} \stackrel{\eqref{eqn:m_partial_omega_q*}}{=} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) δ_\text{ik}\left( x^{ μ}_\text{l} - m^{ μ}_\text{l}\right) \, \text{,}
\end{align}
are used.
With equation \eqref{eqn:tanh_m_patial_theta_q*} and \eqref{eqn:tanh_m_patial_omega_q*} one obtains first order Taylor coefficients,
\begin{align}
\begin{split}
\left. \dfrac{ \partial χ^{μ}_\text{ij}}{ \partial θ_\text{k}} \right| _{ q^{ \ast}} &= \dfrac{1}{2} \int \d \mathbf{x} \left[ \dfrac { \partial p \left( \mathbf{x} | θ ω \right) \left( x_\text{j}^μ - m_\text{j}^μ \right)}{ \partial θ _\text{k}} \underbrace{ \left( \tanh \left( h^{ μ}_\text{i} \right) - m^{ μ}_\text{i} \right)}_{\stackrel{ \eqref{eqn:tanh_h_q*}}{=}0} \right]_{ q^{ \ast}} \\ & \quad + \dfrac{1}{2} \int \d \mathbf{x} \left[ p \left( \mathbf{x} | θ ω \right) \left( x_\text{j}^μ - m_\text{j}^μ \right) \underbrace{ \dfrac{ \partial \left( \tanh \left( h^{ μ}_\text{i} \right) - m^{ μ}_\text{i} \right)}{ \partial θ_\text{k}}}_{\stackrel{ \eqref{eqn:tanh_m_patial_theta_q*} }{=}0} \right]_{ q^{ \ast}} \\
& \quad + ( \text{i} \leftrightarrow \text{j}) \\ &= 0
\end{split} \quad \text{and}
\end{align}
\begin{align}
\begin{split}
\left. \dfrac{ \partial χ^{μ}_\text{ij}}{ \partial ω_\text{kl}} \right| _{ q^{ \ast}} &=  \dfrac{1}{2} \int \d \mathbf{x} \left[ p \left( \mathbf{x} | θ ω \right) \left( x_\text{j}^μ - m_\text{j}^μ \right) \underbrace{\dfrac{ \partial \left( \tanh \left( h^{ μ}_\text{i} \right) - m^{ μ}_\text{i} \right)}{ \partial ω_\text{kl}}}_{ \stackrel{\eqref{eqn:tanh_m_patial_omega_q*}}{=} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) δ_\text{ik}\left( x^{ μ}_\text{l} - m^{ μ}_\text{l}\right)} \right]_{ q^{ \ast}} \\
& \quad + \dfrac{1}{2} \int \d \mathbf{x} \left[ \dfrac { \partial p \left( \mathbf{x} | θ ω \right) \left( x_\text{j}^μ - m_\text{j}^μ \right)}{ \partial ω_\text{kl}}  \underbrace{ \left( \tanh \left( h^{ μ}_\text{i} \right) - m^{ μ}_\text{i} \right)}_{\stackrel{ \eqref{eqn:tanh_h_q*}}{=}0} \right]_{ q^{ \ast}} \\
& \quad + ( \text{i} \leftrightarrow \text{j}) \\
&= \frac{1}{4} \left( 1 - \left( m^{ μ q}_\text{i} \right)^2 \right) δ_\text{ik} δ_\text{jl} + ( \text{i} \leftrightarrow \text{j}) \, \text{.}
\end{split}
\end{align}
Only terms with at least one partial derivative with respect to $ω$ are non-vanishing.
This result we find also within second order MFT approximation of the covariance.
Accordingly there is no contribution linear in $θ$,
\begin{align} \label{eqn:theta_x_q*}
{}^{ θ} χ^{ μ q^{ \ast}}_\text{ij} := \sum _\text{k} \left. \dfrac { \partial χ^{ μ}_\text{ij}}{ \partial θ_\text{k}} \right| _{ q^{ \ast}}  \delta θ_\text{k} = 0 \, \text{.}
\end{align}
Summing over all contributions linear in $δω$ one gets a first order correction to the mean gene expression,
\begin{align} \label{eqn:omega_x_q*}
{}^{ ω} χ^{ μ q^{ \ast}}_\text{ij} := \sum _\text{k} \left. \dfrac { \partial χ^{ μ}_\text{ij}}{ \partial ω_\text{k}} \right| _{ q^{ \ast}}  \delta ω_\text{k} = \frac{1}{4} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) δω_\text{ij} + \left( \text{i} \leftrightarrow \text{j} \right) \, \text{.} 
\end{align}
Plugging in the first order contribution \eqref{eqn:theta_x_q*} and \eqref{eqn:omega_x_q*} into the Taylor expansion \eqref{eqn:mft_taylor_expansion_x} of $χ^{ μ}_\text{ij}$ one obtains the covariance of gene expression in first order MFT,
\begin{align} \label{eqn:χ_mft1}
χ^{ μ \, \text{MFT1}}_\text{ij} = \frac{1}{2} δ_\text{ij} + \frac{1}{4} \left(  1 - \left( m^{μq}_\text{i} \right)^2 \right) ω_\text{ij} + \left( \text{i} \leftrightarrow \text{j} \right) \, \text{.}
\end{align}
The approximation up to second order is conceptually equal to the first order calculation.
The calculation leading to the result,
\begin{align} \label{eqn:χ_mft2}
\begin{split}
χ^{ μ \, \text{MFT2}}_\text{ij} = &+ \frac{1}{2} δ_\text{ij} + \frac{1}{4} \left(  1 - \left( m^{μq}_\text{i} \right)^2 \right) ω_\text{ij} -\frac{1}{2} \left(  1 - \left( m^{ μ q}_\text{j} \right)^2 \right) ω_\text{ji} θ_\text{j}^\text{MFT1}\\
& + \frac{1}{8} \left(  1 - \left( m^{ μ q}_\text{i} \right)^2 \right) \left(  1 - \left( m^{ μ q}_\text{j} \right)^2 \right) \sum_\text{l} ω_\text{il} ω_\text{jl}\\
& + \frac{1}{8} \left(  1 - \left( m^{ μ q}_\text{j} \right)^2 \right) \sum_\text{k} ω_\text{jk} \left(  1 - \left( m^{ μ q}_\text{k} \right)^2 \right)  ω_\text{ki} \\
& - \frac{m^{ μ q}_\text{j}}{6} \left(  1 - \left( m^{ μ q}_\text{j} \right)^2 \right) \left( \vphantom{ \sum_\text{k}} m^{ μ q}_\text{i} \left( ω_\text{ij}\right)^2 \right. \\
& \qquad \left. - 2 ω_\text{ij} δθ_\text{i}^\text{MFT1} - \frac{ m^{ μ q}_\text{j}}{ 2} \sum_\text{k} \left( ω_\text{ik} \right)^2 \right) \\
& + \left( \text{i} \leftrightarrow \text{j} \right) \, \text{,}
\end{split}
\end{align}
is given in the Appendix section \ref{appendix subsection: Covariance of gene expression in second order MFT}.\\

To solve the forward problem within MFT in first order, we iteratively calculate $\mathbf{m}^μ$ and $χ^μ$ according to the set of self-consistent equation \eqref{eqn:m_mft1} and \eqref{eqn:χ_mft1}.
For the calculation within second order we use respectively the equation \eqref{eqn:m_mft2} and \eqref{eqn:χ_mft2}.
A simple iterative procedure with initial values $\mathbf{m}^μ_\text{init} = \mathbf{0}$ and $χ^μ_\text{init} = \mathbb{1}$ converges and thus we obtain a solution to the forward problem of our stochastic model of gene expression within mean field approximation.