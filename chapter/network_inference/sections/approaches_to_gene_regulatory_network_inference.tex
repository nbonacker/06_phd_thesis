
\section{Review of experimental protocols and mathematical models} \label{section:Approaches to gene regulatory network inference}

The challenge of GRNI is to gain information about regulatory relationships based on measurements of gene expression.
The revealing of unknown signalling pathways is based on experimental protocols to quantify gene expression, a mathematical model of the GRN, and the inference of model parameters given gene expression data.

Before we outline mathematical approaches to GRNI based on gene expression data in subsection \ref{subsection: Mathematical models}, we give a brief overview of state-of-the-art experimental protocols to quantify gene expression in the first subsection \ref{subsection: Experimental protocols}.

\subsection{Experimental protocols} \label{subsection: Experimental protocols}

The transcriptome is the set of RNA, which is expressed at a specific moment in an individual cell or a cell group.
Transcriptomics technologies are experimental protocols and corresponding data analysis methods to study the whole or cell-specific transcriptome.

The focus of this section is on protocols quantifying the expression of mRNA.
We neglect ncRNAs, which carry out diverse functions in protein synthesis but are not central for the regulation of gene expression.

The historical evolution of transcriptomics technologies is characterised by breakthrough technologies, opening new horizons and making old techniques obsolete.
In 1991, experiments identified 609 mRNA sequences of the human brain  \cite{Adams1991}.
Currently, complete functional genomics data and transcriptomes of hundreds of organisms under a multitude of different conditions and diseases are publicly accessible \cite{Alvis2018, Yates2019}.

There are two current experimental technologies for the simultaneous measurement of RNA transcription levels.
DNA microarrays quantify the expression of a predefined set of genes \cite{ Schulze2001, Hoheisel2006}, whereas DNA sequencing is principally capable of quantifying the expression level of arbitrary sequences \cite{Wang2010,Chu2018}.\\

The measurement of protein expression in a single cell is technically challenging.
Some protocols have made it possible to quantify the expression of numerous proteins in a single cell \cite{Wu2012}.
Ongoing technological advances increase the coverage and sensitivity of these approaches and make it possible to measure in parallel mRNA and protein concentrations in a single cell \cite{Macaulay2017}.
Combining mRNA and protein expression data could be the starting point for the inference of multi-level gene regulation.
However, the focus of this study is to draw conclusions based on single-level expression data.

\subsubsection{DNA microarray}

In this paragraph, we give a short introduction about DNA microarray technology, and a fully detailed description can be found in the literature \cite{ Schulze2001, Hoheisel2006}.
The idea of DNA microarray technology is the binding of fluorescently labelled single-stranded sequences with complementary probes on the microarray and the measurement of DNA concentration by the fluorescence pattern of the microarray.
The heart of the technology is a microarray chip.
The chip consists of a few up to as many as thousands of DNA probes.
Each probe consists of thousands of identical DNA sequences attached to the solid microarray surface.
One refers to the spatial location of a specific probe on the microarray spot or feature.

The fluorescently labelled DNA sequences, of which one measures the concentration, are called targets.
The targets are applied as a solution onto the microarray chip and bind to their complementary DNA sequences, immobilised on a specific spot.
Binding, the hybridisation between two complementary strands of nucleic acids, is based on non-covalent interactions between the molecules.
One can estimate the concentration of each DNA sequence from the fluorescence pattern after washing the unbound sequences.\\

For the measurement of mRNA concentration, one employs reverse transcription in a preparation step.
Reverse transcription is performed by the reverse transcriptase enzyme, which synthesises complementary DNA (cDNA) from an RNA template.
In a follow-up step, one quantifies cDNA concentration by microarray technology and deduces the mRNA concentration.\\

Approaches based on hybridisation are relatively inexpensive and scalable up to high throughput experiments.
On the downside, DNA microarray has intrinsic limitations.
They are based on a predefined set of probes.
Cross-hybridisation, the formation of double-stranded nucleic acid strains between two molecules with similar but not identical complementary sequences, causes experimental noise.
Furthermore, the comparison of experimental data from different setups is challenging and requires elaborate normalisation methods.

\begin{figure}
	\centering
	\caption{Sequencing with an DNA microarray. Created with BioRender \cite{BioRender}.}
	\includegraphics{chapter/network_inference/graphics/DNA microarray-cropped.pdf}
	\label{figure: DNA microaccay}
\end{figure}

\subsubsection{DNA sequencing}

One can find an extensive review of DNA sequencing technology in the literature \cite{Wang2010,Chu2018, Ambardar2016}, and we give an overview of the involved technology in this paragraph.
Central for the quantification of mRNA expression is the nucleic acid sequence determination of the cDNA.
Therefore mRNA is reverse transcribed into cDNA fragments with adaptors at one end, in the case of single-end sequencing, or both ends, in the case of paired-end sequencing.
After the reverse transcription of the mRNA follows the next-generation sequencing (NGS) of the cDNA.\\

The term NGS is a collective name used to describe different high-throughput sequencing technologies.
The sequencing process can be divided into preparing a nucleic acid source, binding of labelled constituents, and sequence determination.
There are two main approaches to sequencing.
The first approach is sequencing by synthesis (SBS), and the second is sequencing by ligation (SBL).
In figure \ref{figure:Next-generation sequencing} the main steps of SBS and SBL are depicted.
SBS is based on the synthesis of fluorescently labelled nucleotides.
In each sequencing cycle, one nucleotide is added.

SBL uses DNA ligase, an enzyme that facilitates the joining of DNA strands and is sensitive to base-pairing mismatches.
DNA ligase preferentially joins a complementary probe out of a pool of short labelled DNA strands.
Based on the fluorescence pattern, one can reveal the unknown DNA sequence in both sequencing approaches.\\

mRNA sequencing is capable of quantifying single-cell gene expression on account of the small amount of needed mRNA.
Moreover, mRNA sequencing covers an extensive range over which expression levels can be accurately quantified \cite{Mortazavi2008}.
Due to experimental limitations, the quantification of gene expression in single-cell experiments incurs cell destruction.
Therefore the focus in this chapter is on GRNI without time-series data.

\begin{figure}
	\centering
	\caption[Next-generation sequencing.]{Next-generation sequencing of a nucleic acid source by synthesis of flourescently labelled nucleotides and ligation of flourescently labelled oligonucleotides. Created with BioRender \cite{BioRender}.}
	\includegraphics{chapter/network_inference/graphics/DNA sequencing-cropped.pdf}
	\label{figure:Next-generation sequencing}
\end{figure}

\subsection{Mathematical models} \label{subsection: Mathematical models}

The complex system of gene regulation can be modelled as a network.
The regulatory network is composed of nodes, representing genes, and edges, representing regulatory relationships between the genes.
The construction of a mathematical model is the theoretical basis for GRNI.
The biological interpretation of an inferred regulatory relationship heavily depends on the underlying mathematical model.\\

In this subsection, we introduce four major approaches (an information-theoretic model, a Gaussian model, a Bayesian network, and a differential equation model), of which we will combine two models.
In section \ref{section: Stochastic model of gene regulation}, we propose a stochastic model of gene regulation based on a system of differential equations.
Our MLM for the network reconstruction, which we introduce in subsection \ref{subsection: Maximum likelihood method}, is a Gaussian model of gene expression based on the differential equation model.

\subsubsection{Information-theoretic model}

Information-theoretic approaches are based on a measure of statistical dependency.
One assigns the statistical dependency between two expression levels to the corresponding edge in the network.
Measures that are, among others, used in the context of GRNI are correlations and mutual information.

By construction, information-theoretic approaches reveal statistical dependencies, no causal relationships, and do not distinguish between direct and indirect regulatory relationships.
With the use of symmetric measures, the inferred networks are intrinsic undirected.
Information-theoretic approaches reveal the topological structure of GRNs.
On account of the straightforward approach and low computational complexity, they are used in the study of large regulatory networks \cite{Meyer2007}.

Algorithms, which use a local comparison between two-point dependencies, have been proposed to detect indirect interactions and reduce the number of false positive interactions which have a statistical dependence without a direct regular relationship \cite{ Meyer2007, Margolin2006}.

\subsubsection{Bayesian network}

A Bayesian network represents the GRN by a set of genes and their conditional dependencies via a directed acyclic graph.
In contrast to an information-theoretic model, the Bayesian network intrinsically incorporates causal regulatory relationships between genes.
Koller and Friedman provide a comprehensive description of Bayesian networks in their textbook \cite{KollerFriedman2009}.

Within a Bayesian network, one assumes gene expression to be a stochastic process, and random variables, $x_\text{i}$, represent gene expression level.
The regulatory relationships are encoded in a conditional probability distribution, $p(x_\text{i} | π_\text{i})$, where $π_\text{i}$ defines a set of gene expression level of parental genes.
Gene expression is described by a joint probability distribution
\begin{align}
	p(\mathbf{x }) = \prod_\text{i} p(x_\text{i} | π_\text{i}) \, \text{,}
\end{align}
which factorises on account of the assumption of acyclic regulatory relationships.
Inference within a Bayesian network is based on two steps.
The first step is to find an optimal set of parental genes.
The second step is to estimate the functional dependencies that best describe the gene expression data.
The inference based on a Bayesian network is a computationally complex problem.
Still, under the assumption of acyclic regulatory relationships, it is possible to infer causal regulatory interactions within the framework of a Bayesian network \cite{Hill2012,Nee2004}.

\subsubsection{Differential equation model}

Detailed chemical kinetics and spatial models of molecular dynamics increase the understanding of gene regulation \cite{ Schoeberl2002, Aldridge2006}.
On account of complexity, these models are parametrised with many sensitive constants, even for small systems.
Furthermore, parametrisation may depend substantially on the chemical environment and could be entirely different from dilute solution chemistry.

Instead of a detailed biochemical model, a system of differential equations is used as a generic model of a GRN.
In those models time evolution of the gene expression level,
\begin{align}
	\frac{\d x_\text{i}}{ \d t} = f_\text{i}( \mathbf{ x}(t), \bm{ θ}, u_\text{i}(t))  \, \text{,}
\end{align}
is a function of the gene expression level, $\mathbf{x}(t)$, a set of model parameter, $\mathbf{ m}$, and eventually an external perturbation, $u_\text{i}(t)$, which may be time-dependent.
One includes the regulatory interactions and constants of gene expression in the model parameter set, $Θ$.
Inference within a differential equation model is based on a cost function that quantifies how the model describes a given data set and model parameter optimisation. 
It is possible to infer causal regulatory interactions using time series data \cite{Bonneau2006} or steady state gene expression data \cite{Nelander2008} within a differential equation model.

\subsubsection{Gaussian model}

Within the framework of a Gaussian model, statistical relationships between gene expression levels are investigated.
One combines the measurements of gene expression levels at one point of time and under one experimental condition into a vector, $\mathbf{x}$.
This gene expression vector is assumed to be a Gaussian distributed multivariate random variable.
Gene expression is modelled by a multivariate normal distribution,
\begin{align}
	p( \mathbf{x} | \mathbf{m}, \chi) = \frac{1}{ \sqrt{ 2 π \det \chi}} \exp \left( - \frac{1}{2} \left( \mathbf{x} - \mathbf{m} \right)^\text{T} \chi^{-1} \left( \mathbf{x} - \mathbf{m} \right) \right) \, \text{,}
\end{align}
with mean expression level, $\mathbf{m}$, and a covariance matrix, $χ$.
Within the framework of Bayesian inference, one can infer the model parameter, $\mathbf{m}$ and $χ$, and reveal topological information about the GRN encoded in the covariance matrix, $χ$.
Based on gene expression data, undirected regulatory mechanisms can be reconstructed by integration of biological prior information about regulatory mechanisms and prior knowledge about network topology \cite{Li2015}.
On account of the quadratic nature of the Gaussian model, this approach is limited to infer two-point regulatory relationships.

