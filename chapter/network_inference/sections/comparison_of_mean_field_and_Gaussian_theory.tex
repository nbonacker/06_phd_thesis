
\subsection{Comparison of mean field theory and Gaussian theory} \label{comparison_of_mean_field_and_Gaussian_theory}

In this subsection, we discuss the results for $m_\text{i}^\mu$ and $χ_\text{ij}^μ$ within the first two orders MFT and GT.
We investigate the precision of MFT and GT depending on the coupling strength, $β$, which we defined with our stochastic model in section \ref{section: Stochastic model of gene regulation}. 
Furthermore, we show that GT outperforms MFT in the regime of strong inter-gene couplings.

In figure \ref{plots:scatter_forward_problem}, we show scatter plots for the mean, $m_\text{i}^\mu$, and covariance, $χ_\text{i}^\mu$, of gene expression obtained by first and second order MFT, and GT. 
All methods give good results in the regime of weak interaction.
For large inter-gene couplings, we find an overestimation of mean gene expression obtained by first order MFT.
This behaviour is because local fluctuations in the network are neglected, and the system order is overestimated within the mean field approximation.
Correspondingly the variance, $χ^{ μ \, \text{MFT1}}_\text{ii} = \nicefrac{1}{2}$, is underestimated within first order MFT.
We find that in second order MFT, the variance is overestimated. 

To quantify the precision of MFT and GT we use the relative quadratic error between simulated and predicted values of  $m_\text{i}^\mu$ and $χ_\text{ij}^μ$,
\begin{align} \label{eqn:relative_quadratic_error}
r = \sqrt{ \frac{ \sum_\text{i} \left( q_\text{i}^\text{sim} - q_\text{i}^\text{pre} \right) ^ 2}{ \sum_{ \text{i}} \left( q_\text{i}^\text{sim} \right)^2 }} \, \text{.}
\end{align}
For mean and covariance, we plot the $β$-dependence of the relative quadratic error in figure \ref{plots:error_forward_problem}.

The regime of weak inter-gene regulation corresponds to small values of $β$.
Within this regime, one can see that the MFT in first and second order, as well as the GT, give an accurate estimate of mean gene expression, $m_\text{i}^\mu$, and covariance of gene expression, $χ_\text{ij}^μ$.
For large $β$-values, corresponding to the regime of strong inter-gene regulation, MFT breaks down. 
The reason for this breakdown is that the mean field approximation is based on an expansion of $m^{μ}_\text{i}$ and $χ_\text{ij}^μ$ at the factorizing distribution, which is characterised by $ω_\text{ij}^{*} = 0$ corresponding to $β = 0$.

We find that for higher coupling strength, the error in the quadratic contribution outweighs the error made in the linear contribution.
Thus first order MFT outperforms second order MFT in the regime of strong couplings.
The approximation of the external field, $h_\text{i}^\mu$, with a Gaussian distribution is also valid for strong couplings.
The relative quadratic error of $m_\text{i}^\mu$ and $χ_\text{ij}^μ$ within the GT slightly decreases with increasing $β$.
Therefore GT outperforms MFT in a regime of strong inter-gene coupling.

\begin{figure}
	\caption[ Scatter plot of mean and covariance for first and second order MFT and GT.]{ Scatter plot of mean, $m_\text{i}^\mu$, and covariance, $χ_\text{i}^\mu$, for first order MFT \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, second order MFT \tikz \definecolor{MyBlue}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, and GT \tikz \definecolor{MyBlue}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);.
	For the generation of the interaction matrix a medium inter-gene coupling strength, $β = 0.5$, and a large inter-gene coupling strength, $β = 1.0$, are used.}
	\label{plots:scatter_forward_problem}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\caption[]{  $β$ = 0.5}
		\includegraphics{chapter/network_inference/graphics/scatter_mean_β=0,5-cropped.pdf}
		\label{plots:scatter_mean_β=05}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\caption[]{  $β$ = 0.5}
		\includegraphics{chapter/network_inference/graphics/scatter_covariance_β=0,5-cropped.pdf}
		\label{plots:scatter_covariance_β=05}
	\end{subfigure} \\
	\begin{subfigure}{0.48\textwidth}
		\caption[]{  $β$ = 1.0}
		\includegraphics{chapter/network_inference/graphics/scatter_mean_β=1,0-cropped.pdf}
		\label{plots:scatter_mean_β=1}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\caption[]{  $β$ = 1.0}
		\label{scatter_covariance_β=1}
		\includegraphics{chapter/network_inference/graphics/scatter_covariance_β=1,0-cropped.pdf}
	\end{subfigure}
\end{figure}

\begin{figure}
	\caption[Relative quadratic error of mean and covariance]{ Relative quadratic error of mean and the covariance of gene expression for first order MFT \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, second order MFT \tikz \definecolor{MyBlue}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, and the GT \tikz \definecolor{MyBlue}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);.}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\caption[]{Error of mean gene expression.}
		\includegraphics{chapter/network_inference/graphics/error_m_β=var_09-cropped.pdf}
		\label{ plots:m_05}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\caption[]{Error of gene expression covariance.}
		\includegraphics{chapter/network_inference/graphics/error_x_β=var_09-cropped.pdf}
		\label{ plots:χ_05}
	\end{subfigure} \label{plots:error_forward_problem}
\end{figure}