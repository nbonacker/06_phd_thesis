
\section{Stochastic model of gene regulation} \label{section: Stochastic model of gene regulation}

We propose our stochastic model of gene regulation, a system of stochastic differential equations in this section.

The motivation to employ stochastic differential equations to model a GRN is that gene expression is an intrinsic stochastic process.
Randomness in transcription and translation leads to significant fluctuations in mRNA and protein levels \cite{Raj2008}.
Moreover, we try to gain additional information out of correlations between fluctuations in gene expression levels for the network reconstruction.
Within this study, we focus on GRNI based on steady state gene expression data.
Therefore, we focus on the steady state distribution of our stochastic model of gene regulation.\\

Within subsection \ref{subsection:Stochastic model of gene regulation}, we define the stochastic model of gene expression.
We characterise the steady state of gene expression within our model in subsection \ref{subsection:Steady state of our stochastic model of gene regulation}.
In the closing subsection \ref{subsection:Connection to the asymmetric Ising model}, we point out that our proposed model has a similar steady state characteristic as the asymmetric Ising model.
The gene expression levels and the spin moments obey similar algebraic relations in the steady state.
On account of this connection, we extend in the following section methods developed in the context of the asymmetric Ising model to solve the forward problem of our stochastic model of gene regulation.

\subsection{Stochastic model of gene regulation} \label{subsection:Stochastic model of gene regulation}

We employ a stochastic version of a deterministic model developed in the context of GRNI by Nelander \cite{Nelander2008}.
In this chapter the system of stochastic differential equation,
\begin{align} \label{eqn:stochastic_differential_equations}
\frac{ \text{d} x_\text{i}^μ}{\text{d} t} = a_\text{i} \tanh \left( h_\text{i}^\mu \right) - b_\text{i} x_\text{i}^\mu + c_\text{i} \xi_\text{i} \text{,}
\end{align}
is used to model the dynamics of gene regulation.
The time evolution of gene expression levels is modelled with a system of non-linear differential equations, comparable to a Hopfield network in the field of artificial neural networks \cite{HertzKroghPalmer91}.
The network is composed of a set of nodes, $x_\text{i}^\mu$, representing the gene expression level.
The gene expression is regulated by an external field,
\begin{align}
h_\text{i}^\mu = \sum_{ \text{k}} \omega_{ \text{ik}} x_\text{k}^\mu + \theta_\text{i} + u_\text{i}^\mu \, \text{,}
\label{eqn:definition_external_field}
\end{align}
which is called a synaptic field in the context of neuronal networks, and proportional to a gene specific expression coefficient, $a_\text{i}$.
The external field depends on the expression level, $x_\text{k}^\mu$, via the interaction matrix, $\omega_{ \text{ik}}$, a threshold vector, $θ_\text{i}$, and a perturbation vector, $u_\text{i}^\mu$.
The exponent $μ$ labels different perturbations and corresponding trajectories of gene expression, $\mathbf{x}^{μ}(t)$.
Within this model, we assume an exponential decay of mRNA or protein concentration, which is quantified by decay constant $b_\text{i} > 0$.

Gene expression and the decay of the gene products are intrinsic stochastic processes because of the relatively small number of specific gene products.
Random fluctuations in transcription and translation cause significant cell-to-cell variations in gene product concentrations \cite{Raj2008, Elowitz2002}.
Mathematically we model the stochastic nature of gene expression with a rapidly fluctuating term, $\xi_\text{i}$, defined by zero mean \eqref{eqn:time_average_ξ} and correlation \eqref{eqn:time_average_ξξ}.
The magnitude of the stochastic contribution is quantified by the constant $c_\text{i} > 0$.

We assume the external field to be linear in the gene expression level.
Therefore, only two-gene interactions are considered.
We do not incorporate the phenomenon of co-activation nor co-repression, which are three-gene interactions.

Nevertheless, the model can represent essential aspects of GRNs, including oscillatory states, saturation, and homeostasis of gene expression.
In figure \ref{figure: time evolution} a plot of the simulated time evolution of our stochastic model of gene regulation is shown.

There is no reasonable argument for symmetric interactions between individual genes.
The amplification of gene expression by another gene does not imply a reverse amplification.
Such that the interaction is typically asymmetric, and our model violates the detailed balance relation \eqref{equation:detailed balance relation}.
Therefore, there is no reasonable argument for the assumption that gene expression levels are distributed according to a Boltzmann distribution.
On account of the absence of a Boltzmann distribution, we base our network inference on the dynamic model \ref{eqn:stochastic_differential_equations}.

\begin{figure}
	\centering
	\captionabove[]{Simulated time evolution of gene expression level for a fully connected GRN as small as 10 nodes, where the model parameter are are set to $a_\text{i} = 1, b_\text{i} = 1$ and $ c_\text{i} = 0.1$. For the generation of the interaction matrix, $ω$, the coupling strenght, $β = 0.5$, is used.} \label{figure: time evolution}
	\includegraphics{chapter/network_inference/graphics/time_evolution-cropped.pdf}
\end{figure}

To test our approach to GRNI, we simulate the time evolution according to generated model parameters and take random samples after the system has reached a steady state.

For the generation of the gene interaction matrix, $ω$, we set $ \forall \text{i} \neq j : ω_\text{ij} = β ζ_\text{ij}$, where $ζ_\text{ij}$ is a random variable drawn independently from a normal distribution with mean equal to zero and variance equal to the inverse of total numbers of genes, $\nicefrac{1}{N}$.
Thus, assuming independent, $x_\text{j}$, the variance of the external field, $h_\text{i}$, would be N-independent.

The constant $β$, the inter-gene coupling strength, is a measure of the regulation strength between genes.
Such a generated GRN is referred to as a fully connected network, in contrast to a sparse network where there are only a few non-zero matrix elements, $ω_\text{ij}$.
In our proof-of-principle study, self-regulation is not considered, $ \forall \text{i} : ω_\text{ii} = 0$.

\subsection{Steady state of our stochastic model of gene regulation} \label{subsection:Steady state of our stochastic model of gene regulation}

We characterise the state of our stochastic model with a multivariate probability distribution of gene expression levels.
Because of the focus on steady state gene expression data, we study in this subsection the steady state distribution of our model.
We describe the distribution of gene expression level with mean vector, $\mathbf{m}^\mu$, and covariance matrix, $\chi^\mu$, defined by
\begin{align}
m_\text{i}^\mu = \left< x_\text{i}^\mu \right> \quad \text{and} \quad \chi_\text{ij}^\mu = \left< x_\text{i}^\mu x_\text{j}^\mu \right> - \left< x_\text{i}^\mu \right> \left< x_\text{j}^\mu \right> \, \text{.}
\label{eqn:definition_first_and_second_moment}
\end{align}
The calculation of mean, $\left< x_\text{i}^\mu \right>$, and two-point correlation function, $\left< x_\text{i}^\mu x_\text{j}^\mu \right>$, is based on the stochastic differential equation \eqref{eqn:stochastic_differential_equations},
\begin{align} \label{eqn:ddtx}
\ddt x_\text{i}^\mu = F_\text{i}(\mathbf{x}^μ) +	c_\text{i} \xi_\text{i} \, \text{.}
\end{align}
The first term on the right hand side of the equation \eqref{eqn:ddtx} describes the deterministic contribution to dynamics,
\begin{align} \label{eqn:definition_F}
F_\text{i}(\mathbf{x}^μ) = a_\text{i} \tanh \left( \sum_{ \text{k}} \omega_{ \text{ik}} x_\text{k}^\mu + \theta_\text{i} + u_\text{i}^\mu \right) - b_\text{i} x_\text{i}^\mu \, \text{.}
\end{align}
For the calculation of $\left< x_\text{i}^\mu \right>$ the steady state average of the time derivative of gene expression \eqref{eqn:ddtx} is set equal to zero and one receives the mean gene expression in the steady state,
\begin{align}
\begin{split}
0 &= \left< F_\text{k}(\mathbf{x}^μ) \right>_\text{s} \\
\iff \left< x_\text{i}^\mu \right>_\text{s} &= \frac{ a_\text{i}}{ b_\text{i}} \left< \tanh \left( h_\text{i}^\mu \right) \right>_\text{s} \, \text{,}
\end{split}
\end{align}
where $ \left< x_\text{i}^\mu \right>_\text{s} = \text{const.}$ and $ \left< \xi_\text{i} \right>_\text{s} \stackrel{ \eqref{eqn:time_average_ξ}}{=} 0$ is used.
For the calculation of $\left< x_\text{i}^\mu x_\text{j}^\mu \right>$, one starts with the total time derivative of $f_\text{ij}( \mathbf{x}^μ) = x_\text{i}^\mu x_\text{j}^\mu$.
The many variables version of Ito´s formula \eqref{eqn:many_variable_itos_formula} is used and one obtains a total time derivative,
\begin{align} \label{eqn:ddtxx}
\ddt \, ( x_\text{k}^\mu x_\text{l}^\mu) = F_\text{k}(\mathbf{x}^μ)x_\text{l}^\mu + F_\text{l}(\mathbf{x}^μ)x_\text{k}^\mu + δ_\text{kl} c_\text{l}^2 + c_\text{k} x_\text{l}^\mu  \xi_\text{k} + c_\text{l} x_\text{k}^\mu  \xi_\text{l}\, \text{.}
\end{align}
Similar to the calculation of mean gene expression one performs the steady state time average over the total time derivative \eqref{eqn:ddtxx} and yields
\begin{align}
\begin{split}
0 &= \left< F_\text{k}(\mathbf{x}^μ)x_\text{l}^\mu + F_\text{l}(\mathbf{x}^μ)x_\text{k}^\mu \right>_\text{s} + δ_\text{kl} c_\text{k}^2 \\ 
\iff \left< x_\text{i}^\mu x_\text{j}^\mu \right>_\text{s} &= \frac{ a_\text{i}}{ b_\text{i} + b_\text{j}} \left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu \right>_\text{s} + \frac{1}{2} \frac{c_\text{i}^2}{  b_\text{i} + b_\text{j}} \delta_\text{ij} + \left( \text{i} \leftrightarrow \text{j} \right) \, \text{,}
\end{split}
\label{eqn:steady_state_average}
\end{align}
where $\left< x_\text{l}^\mu  \xi_\text{k} \right>_\text{s} \stackrel{\eqref{eqn:mean_value_formula}}{=} 0 $ for the non-anticipating variable $x_\text{l}^\mu$ is used. The term $\left( \text{i} \leftrightarrow \text{j} \right)$ indicates a summand with interchanges indices.

For the solution of the forward problem within mean field approximation, the three point correlation function, $\left< x_\text{i}^\mu x_\text{j}^\mu x_\text{k}^\mu \right>_\text{s}$, turns out to be useful for the calculation of the covariance matrix, $χ^{ μ}_\text{ij}$.
The calculation of $\left< x_\text{i}^\mu x_\text{j}^\mu x_\text{k}^\mu \right>_\text{s}$ is completely analogue to the calculation of $\left< x_\text{i}^\mu x_\text{j}^\mu \right>$, thus the multivariable version of Ito´s formula is applied to the function $f_\text{ijk}( \mathbf{x}^μ) = x_\text{i}^\mu x_\text{j}^\mu x_\text{k}^\mu$ and the steady state average is taken.
Collecting the results, the first three moments of the steady state distribution are given by the following set of equations
\begin{align}
\begin{split} \label{eqn:exact_relations}
\left< x_\text{i}^\mu \right>_\text{s} &= \frac{ a_\text{i}}{ b_\text{i}} \left< \tanh \left( h_\text{i}^\mu \right) \right>_\text{s}  \\
\left< x_\text{i}^\mu x_\text{j}^\mu \right>_\text{s} &= \frac{ a_\text{i}}{ b_\text{i} + b_\text{j}} \left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu \right>_\text{s} + \left( \text{i} \leftrightarrow \text{j} \right) + \frac{c_\text{i}^2}{  b_\text{i} + b_\text{j}} \delta_\text{ij} \\
\left< x_\text{i}^\mu x_\text{j}^\mu x_\text{k}^\mu \right>_\text{s} &= \frac{ a_\text{i}}{ b_\text{i} + b_\text{j} + b_\text{k}}  \left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu x_\text{k}^\mu \right>_\text{s} + \frac{c_\text{i}^2}{ b_\text{i} + b_\text{j} + b_\text{k}} \delta_\text{ij} \left< x_\text{k}^\mu \right>_\text{s} \\ 
& \quad + \left( \text{i} \leftrightarrow \text{j} \leftrightarrow \text{k}  \right)
\, \text{,}
\end{split}
\end{align}
where $\left( \text{i} \leftrightarrow \text{j} \leftrightarrow \text{k}  \right)$ indicates summands with cyclic permutations of indices.

\subsection{Connection to the asymmetric Ising model} \label{subsection:Connection to the asymmetric Ising model}

We begin this subsection with an outline of the symmetric Ising model before we focus on the asymmetric Ising model and choose a dynamics.
The chosen dynamics leads to a non-equilibrium steady state.
We show that spin moments in the steady state of the asymmetric Ising model are similar to the moments of steady state gene expression of our stochastic model of gene regulation.\\

The Ising model is named after Cologne-born Ernst Ising. He studied the symmetric model in his PhD thesis and solved it in one dimension, proving that there exists no phase transition in the one-dimensional Ising model \cite{Ising1925}.
His eventful life was marked by racist oppression by the National Socialists, but also by great kindness and joy in teaching students \cite{ Kobe2000}.

The free parameter of the symmetric Ising model are binary spin-variables, $s_\text{i} \in \{−1, 1 \}$, and the Hamiltonian is given by
\begin{align} \label{equation:Ising model Hamiltonian}
\mathcal{H}( \mathbf{s})=-\frac{1}{2} \sum_\text{ij} J_\text{ij} s_\text{i} s_\text{j}-\sum_\text{i} \theta_\text{i} s_\text{i}
\end{align}
with local magnetic field, $θ_\text{i}$, and symmetric interactions, $J_\text{ij} = J_\text{ji}$.
The equilibrium statistics of the symmetric Ising model are described by the Bolzmann distribution,
\begin{align}
\begin{split}
p( \mathbf{s}) &= \frac{1}{Z} \exp \left( -β \mathcal{H}( \mathbf{s}) \right) \, \text{, where} \\
Z &= \sum_\mathbf{s} \exp \left( -β \mathcal{H}( \mathbf{s}) \right)
\end{split}
\end{align}
is the partition function of the system.\\

A common choice to introduce a time evolution is sequential Glauber dynamics.
Within sequential Glauber dynamics one assumes discrete time steps and in every time step a random spin, $s_\text{i}$, is updated according to the probability distribution
\begin{align} \label{equation:time_evolution_sequential_glauber_dynamics}
\begin{split}
p( s_\text{i}( t + 1) | s(t)) &= \frac{ \exp \left( β s_\text{i}( t + 1) h_\text{i}( t)\right) }{ 2 \cosh \left( β h_\text{i}( t) \right) } \, \text{.}
\end{split}
\end{align}
Analogously to our stochastic model of gene expression a local effective field acting on spin i is defined as $ h_\text{i}( t) = \sum_\text{j} J_\text{ij} s_\text{j}(t) + \theta_\text{i}$.
The time evolution according to parallel Glauber dynamics is discussed in the Appendix section \ref{appendix section: Parallel Glauber dynamics}.\\

An asymmetric interaction matrix characterises the asymmetric Ising model.
For the asymmetric Ising model, both choices of dynamics ( sequential as well as parallel Glauber dynamics) converge to a non-equilibrium steady state \cite{Coolen2000a}, which does not hold the detailed balance relation \eqref{equation:detailed balance relation}.
The quantification of the non-equilibrium steady state of the asymmetric Ising model is a hard problem.
However one can derive a set of self-consistent relations for the first moments of the steady state distribution.
We obtain for the first moment of the steady state distribution the relation
\begin{align} \label{eqn:exact_relation_first_order_asymetric_Ising_model}
\begin{split}
\left < s_\text{i}( t+1) \right> &\stackrel{ \eqref{equation:time_evolution_sequential_glauber_dynamics}}{=} \frac{1}{N} \left< \tanh( h_\text{i}( t)) \right> + \frac{N-1}{N} \left< s_\text{i}( t) \right> \\
\Rightarrow \left< s_\text{i} \right>_\text{s} &\stackrel{ \phantom{ \eqref{equation:time_evolution_sequential_glauber_dynamics}}}{=} \left< \tanh( h_\text{i}) \right>_\text{s} \, \text{,}
\end{split}
\end{align}
where in any time step one out of $N$ spins is updated according to the sequential update rule \eqref{equation:time_evolution_sequential_glauber_dynamics}.
The averages become time independent in the steady state.
We obtain the analogous relation
\begin{align}
\begin{split}
\left< s_\text{i}( t+1) s_\text{j}( t+1) \right> &\stackrel{ \eqref{equation:time_evolution_sequential_glauber_dynamics}}{=} \frac{1}{N} \left< \tanh( h_\text{i}( t)) s_\text{j}( t)\right>\\
& \qquad + \frac{1}{N} \left< \tanh( h_\text{j}( t)) s_\text{i}( t)\right> \\ 
& \qquad + \frac{N-2}{N} \left< s_\text{i}( t) s_\text{j}( t) \right>\\
\Rightarrow \left< s_\text{i} s_\text{j} \right>_\text{s} 
&\stackrel{ \phantom{ \eqref{equation:time_evolution_sequential_glauber_dynamics}}}{=} \frac{1}{2} \left< \tanh( h_\text{i}) s_\text{j}\right>_\text{s} + \frac{1}{2} \left< \tanh( h_\text{j}) s_\text{i}\right>_\text{s} \, \text{.}
\end{split}
\end{align}
for the two-point correlation function in the steady state.

An important property in the context of this project is that the asymmetric Ising model equipped with sequential Glauber dynamics has structurally similar steady state relations as our model of stochastic gene expression \eqref{eqn:exact_relations}.
This is also true for the first moment with parrallel Glauber dynamics \eqref{eqation:exact_relation_first_order_parallel_glauber_dynamics}, whereas the second moment $ \left< s_\text{i} s_\text{j} \right> = \left< \tanh( h_\text{i}) \tanh( h_\text{j})\right> $ has a slightly different structure.
The calculation for parallel Glauber dynamics is presented in the Appendix section \ref{appendix section: Parallel Glauber dynamics}.

We will use the connection between these models in the next section.
There, we extend ideas developed in the context of asymmetric Ising models to solve the forward problem of our stochastic model of gene expression.
