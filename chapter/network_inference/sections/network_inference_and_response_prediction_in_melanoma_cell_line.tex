
\section{Inference and response prediction in a melanoma cell line} \label{section:Inference and response prediction in a melanoma cell line}

We focus in this section on the inference based on experimental perturbation data.
We employ the four approaches introduces in the previous section based on gene expression measurements in the melanoma cell line SK-MEL-133.
We find that a prediction based on inferred model parameter gives an accurate estimate of unknown gene expression level.
For our likelihood-based approach, we show a graph representation of the inferred interaction matrix and compare the reconstructed GRN to the literature.\\

Before we start with the response prediction we outline the experimental setup in subsection \ref{subsection:SK-MEL-133 perturbation data}.
We divide the perturbation data up into training and prediction set.
Based on inference within the training set we predict gene expression for the prediction set in subsection \ref{subsection:Response prediction}.
In the closing subsection \ref{subsection:Network reconstruction} we employ our likelihood-based approach on the whole dataset and discuss the inferred regulatory interactions.

\subsection{Perturbation experiment} \label{subsection:SK-MEL-133 perturbation data}

The perturbation experiment in the SK-MEL-133 cell line is published in the supporting information of the publication \cite{Molinelli2013}.
The SK-MEL-133 melanoma cell line has functional mutations within the MAPK pathway, which is discussed in the motivational section \ref{section:Motivation to gene regulatory network inference} as an important pathway in the development and spread of melanoma.

The SK-Mel-133 cells are singularly and pairwise perturbed using a set of 8 inhibitors, which predominantly target the PI3K/AKT and MAPK pathway.
As an inhibitory concentration the $\text{IC}_{40}$ value is used to generate a gentle perturbation with a measurable effect.
The $\text{IC}_{40}$ value is the minimal concentration required to reduce the expression of the targeted gene by 40\%, which can be estimated from a dose-response curve.

Within a set of 16 proteins, the response in gene expression is measured.
This set of proteins does not include the perturbed genes, which is a fundamental difference to the inference based on simulated data in the previous section.
For each of the 44 perturbations, three independent biological replicates are measured and response is quantified by a logarithmic ratio between perturbed and unperturbed expression levels.

Gene expression levels are measured with reverse-phase protein arrays (RPPA).
RPPA is a microarray technology.
DNA microarray technologies are outlined in section \ref{subsection: Experimental protocols}.
The RPPA technology is designed for the simultaneous measurement of protein concentration and phosphorylation state in a large number of biological samples \cite{Tibes2006}.

Apart from the measurement of gene expression level, the cell viability after drug perturbation is quantified 72 hours after perturbation in a resazurin assay.
Resazurin is a weakly blue fluorescent, cell-permeable substance. 
It is irreversibly reduced to the pink-coloured and highly fluorescent resorufin by metabolic cell activity.
Therefore the reduction of resazurin is a widely used indicator of cell proliferation and viability.

\subsection{Response prediction} \label{subsection:Response prediction}

We divide the 44 perturbations up into training sets with 33 perturbations and corresponding prediction sets consisting out of 11 perturbations.
Such that there are in total four pairs of distinct training and prediction sets.
Based on training data we infer the gene interaction matrix, $ω$, and the model parameter $\mathbf{a}$, $\mathbf{b}$, and $\mathbf{c}$.

We employ the four inference approaches introduced in the previous section.
Compared to the previous section, we enlarge the parameter space by the set of model parameter $\mathbf{a}$, $\mathbf{b}$, and $\mathbf{c}$.
Matrix elements, $ω_\text{ij}$, are set equal to zero that quantify the regulatory effect on the perturbed genes and for which there are no gene expression measurements.
Such that $ω_\text{ij} = 0$ for all $\text{i}$ labelling the perturbed genes.

There exist regions of the parameter space where the GT does not converge.
Moreover, we find that the success of our MLM depends on the starting point of the local optimisation.
We obtain reliable predictions by using the result of the least squares fit based on GT as a starting point for our MLM.

Given the inferred interaction matrix and model parameter we predict mean gene expression, $\mathbf{m}^μ$, within the prediction set using a numerical simulation based on our stochastic model of gene expression.

For the first pair of training and prediction set, the predicted gene expression level are compared to the measured ones in figure \ref{figure:prediction_t=1_n=1}.
The predictions based on the other training sets are depicted in the Appendix section \ref{appendix section: Response prediction}.

We find that gene expression levels can be predicted based on our stochastic model of gene regulation.
Based on all four sets of training and prediction data, we obtain for the least squares method in first order a mean square error of $r_\text{ls1o}= 0.8 \pm 1.2$ and in second order of $r_\text{ls2o}= 1.3 \pm 2.1$.
Whereas the mean square error of $r_\text{lsGt}= 0.086 \pm 0.020$ for the least squares Gaussian theory approach and our likelihood based method $r_\text{mlGt}= 0.083 \pm 0.014$ are significant smaller.

\iffalse

m=1 mean=0.8249243772162179
m=1 std=1.1678257359238042
m=2 mean=1.3548614128950622
m=2 std=2.0181337280596288
m=3 mean=0.08614810452537415
m=3 std=0.019410687660684805
m=4 mean=0.08339344124268244
m=4 std=0.013877319623995153

\fi

\begin{figure}
	\centering
	\captionabove[]{Scatter plot of predicted and measured gene expression level.
		The inference is based on a training set, which does not include the predicted perturbations.
		We employ the least squares fit in first order, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, in second order, \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, and based on the GT, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);.
		We employ furthermore our MLM, \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);.
		We give the mean squared distances between predicted and measured gene expressions.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ $r_\text{ls1o} = 0.125$} 
		\includegraphics{chapter/network_inference/graphics/prediction_m=1_t=1_n=1-cropped.pdf}
		\label{subfigure:prediction_m=1_t=1_n=1}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ $r_\text{ls2o} = 0.182$} 
		\includegraphics{chapter/network_inference/graphics/prediction_m=2_t=1_n=1-cropped.pdf}
		\label{subfigure:prediction_m=2_t=1_n=1}
	\end{subfigure} \\
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ $r_\text{lsGt} = 0.095 $}
		\includegraphics{chapter/network_inference/graphics/prediction_m=3_t=1_n=1-cropped.pdf}
		\label{subfigure:prediction_m=3_t=1_n=1}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ $r_\text{mlGt} = 0.092$}
		\includegraphics{chapter/network_inference/graphics/prediction_m=4_t=1_n=1-cropped.pdf}
		\label{subfigure:prediction_m=4_t=1_n=1}
	\end{subfigure}
	\label{figure:prediction_t=1_n=1}
\end{figure}

\subsection{Network reconstruction} \label{subsection:Network reconstruction}

For the network reconstruction, we employ our likelihood-based method, which we introduce in subsection \ref{subsection: Maximum likelihood method}, based on the whole dataset.
To incorporate the knowledge that most of the genes do not interact, we use the heavy-tailed Laplace distribution, as a sparsity-favouring prior distribution.
We employ the Laplace distribution with mean, $ν = 0$, and variance, $ 2σ^2= \nicefrac{1}{2}$ as a prior for the matrix elements, $ω_\text{ij}$.
In this way, $ω_\text{ij}$ is set equal to zero whenever there is no clear evidence in the data for a gene interaction.
On the other hand $ω_\text{ij}$ which are required are allowed to be sizeable, because the Laplace distribution decreases slightly with $\exp (-|x|)$, whereas the normal distribution decreases with $\exp (-x^2)$.
We do not take additional prior information for the parameter $\mathbf{a}, \mathbf{b}, \text{ and }\mathbf{c}$ into account and assume a flat prior distribution for these parameters.
Finally, we obtain a maximum posterior estimate based on the GT,
\begin{align}
	\begin{split}
		\{ ω, \mathbf{a}, \mathbf{b}, \mathbf{c} \}_\text{mpGt} &= \argmax_{ \{ω, \mathbf{a}, \mathbf{b}, \mathbf{c} \} } \, \left( -\frac{1}{2} \sum_{ \mu \text{k}} \left( ^\text{data}\mathbf{x}_\text{k}^\mu - \mathbf{m}^\mu( ω, \mathbf{a}, \mathbf{b}, \mathbf{c})  \right) ^\text{T} \right. \\
		& \quad \quad  \left( \chi^\mu( ω, \mathbf{a}, \mathbf{b}, \mathbf{c}) \right)^{-1} \left( ^\text{data}\mathbf{x}_\text{k}^\mu - \mathbf{m}^\mu( ω, \mathbf{a}, \mathbf{b}, \mathbf{c} ) \right) \\
		& \quad  \left. -\frac{1}{2} \ln \det \left( \chi^\mu( ω, \mathbf{a}, \mathbf{b}, \mathbf{c}) \right) - \sum_\text{ij} \frac{ | ω_\text{ij} - ν| }{σ} \right) \, \text{,}
	\end{split}
\end{align}
for the model parameter. 

In figure \ref{figure:network_inf-cropped} we give a graph representation of the inferred subnetwork of the measured proteins.
We employ a lower limit of $0.2$ on the absolute value for the representation of an regulatory interaction.

Our network is more complex than most signalling cascades in the literature.
Nonetheless, we compare our findings, the inferred signalling network, to the manually curated, open access Reactome pathway database \cite{Reactome2020}.
The regulatory interactions within the database are represented in figure \ref{figure:network_ref-cropped}.
In table \ref{table:Confusion table} we compare our inferred interactions with the data base.
We find that the inferred regulatory interactions do not represent known regulatory relationships.

The high ratio between the number of genes and the sample size, and probably also the experimental noise, make the inference of biochemical interactions without integration of prior knowledge infeasible.

\renewcommand{\arraystretch}{1.2}

\begin{table}
	\begin{center}
		\caption{Confusion table between interaction from the Reactome pathway database \cite{Reactome2020} and inferred interaction based on our MLM. The reference interaction are classified as activating, non-interacting, inhibiting, and undirected interaction.}
		\label{table:Confusion table}
		\begin{tabular}{c|c|c|c|c}
			\diagbox{inf.}{ref.} & act. & \makecell{ non- \\ int.} & inh. & \makecell{ und. \\ int.} \\ \hline
			$ω_\text{ij} \ge 0.2$ & 0 & 12 & 0 & 2 \\ \hline
			$ -0.2 \ge ω_\text{ij} \ge 0.2$ & 26 & 175 & 2 & 20 \\ \hline
			$ω_\text{ij} \le -0.2$ & 1 & 14 & 1 & 3
		\end{tabular}
	\end{center} 
\end{table}

\begin{figure}
	\centering
	\captionabove[]{The graph represents our maximum posterior estimate for the GRN. We plot inferred regulatory relationships with $|ω_\text{ij}| \ge 0.2$. The graph representation is created with Cytoscape \cite{Cytoscape}.}
	\includegraphics[width=\textwidth]{chapter/network_inference/graphics/network_inf-cropped.pdf}
	\label{figure:network_inf-cropped}
\end{figure}

\begin{figure}
	\centering
	\captionabove[]{The graph represents the reference network based on the pathway database Reactome \cite{Reactome2020}. Shown are activating, $\rightarrow$, inhibiting, $\dashv$, and undirected interactions, $-$. The dashed lines represent predicted interactions. The graph representation is created with Cytoscape \cite{Cytoscape}.}
	\includegraphics[width=\textwidth]{chapter/network_inference/graphics/network_ref-cropped.pdf}
	\label{figure:network_ref-cropped}
\end{figure}