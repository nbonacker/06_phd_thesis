

\section{Inverse problem} \label{section:Inverse problem}

The inverse problem of GRNI is solved within our stochastic model of gene regulation.
We employ two generic approaches, least squares fits and our MLM.
We find that our likelihood-based approach outperforms the least squares fits in the regime of strong stochastic contribution to the system dynamics.\\

The least squares fits, which are introduced in subsection \ref{subsection: Least squares methods}, are based on simple quadratic cost functions.
These cost functions quantify the accurateness of a given set of model parameters to describe steady state data.
The cost functions are based on the exact relations of the first and second moment of the steady state distribution \eqref{eqn:exact_relations} and the results of the GT.
The inference within least squares methods takes place without a concrete assumption on a statistical model of gene expression fluctuation.

Within our MLM, which is introduced in subsection \ref{subsection: Maximum likelihood method}, we employ the framework of statistical inference.
We assume the gene expression level to be distributed according to a multivariate Gaussian distribution characterised by the results of the forward problem, $\mathbf{m}^\mu( ω)$ and $χ^\mu( ω)$.
Under the presumption of no prior information, we obtain an interaction matrix with a maximum likelihood estimate.

For the minimisation within the least squares fits and respectively the likelihood maximisation based on simulated data, we tested local and global optimisation algorithms implemented within the NLopt library for non-linear optimisation \cite{NLopt}.
We find no clear evidence for the benefit of global optimisation over a local optimisation starting at the origin, $\forall \text{i,j}: ω_\text{ij}^\text{init}=0$.

Within our study, we employ bound optimisation by quadratic approximation (BOBYQA) \cite{Powell2009}.
The BOBYQA algorithm is a realisation of a local gradient-free optimisation algorithm.
In each iteration step of the algorithm a local quadratic approximation, $q$, of the function to maximise, $f$, is computed.
The interpolation points are updated by minimizing the Frobenius norm of the second derivative matrix of $q$.
No derivatives of $f$ are required explicitly, thus an implementation of a gradient is not necessary.

The results of GRNI based on simulated data within least squares fits and our MLM are discussed in subsection \ref{subsection: Comparison of least squares and maximum likelihood method}.

\subsection{Least squares methods} \label{subsection: Least squares methods}

Least squares fits are used in the reconstruction of GRNs as big as 100 nodes based on perturbation data \cite{ Molinelli2013, Korkut2015}.
The inferred GRNs confirm and extend the knowledge about biological pathways and accurately predict the outcome of untested perturbations.

We employ a least squares fits based on the steady state relation of the first moment \eqref{eqn:exact_relations}.  
To obtain an estimate for the interaction matrix, 
\begin{align}
ω_\text{ms1o} = \argmin_ω \left( \sum_{ \mu \text{i}} \left[ \left< x_\text{i}^\mu \right> - \frac{ a_\text{i}}{ b_\text{i}} \left< \tanh \left( h_\text{i}^\mu( \mathbf{x}^\mu, ω) \right) \right>  \right]^2 \right) \, \text{,}
\end{align}
a quadratic cost function is minimised with respect to $ω$.
The averages in the cost function are performed over the steady state data.
Discrepancies between mean gene expression, $\left< x_\text{i}^\mu \right>$, and expected expression in the steady state, $\nicefrac{ a_\text{i}}{ b_\text{i}} \left< \tanh \left( h_\text{i}^\mu( \mathbf{x}^\mu, ω) \right) \right>$, are penalised quadratically.
Therefore, a lower cost interaction matrix represents the data more accurately.

The solution of the forward problem within GT intrinsically incorporates the second moment of the gene expression distribution.
Therefore, we also examine the information contained in the second moments of the samples with a least squares method.
The steady state relation of first and second moments \eqref{eqn:exact_relations} is used to infer the interaction matrix, $ω_\text{ms2o}$, within a mean square approach in second order,
\begin{align}
	\begin{split}
		ω_\text{ms2o} &= \argmin_ω \left( \sum_{ \mu \text{i}} \left[ \left< x_\text{i}^\mu \right> - \frac{ a_\text{i}}{ b_\text{i}} \left< \tanh \left( h_\text{i}^\mu( \mathbf{x}^\mu, ω) \right) \right>  \right]^2 \right. \\
		& \quad \left. + \frac{1}{N} \sum_{ \mu \text{ij}} \left[ \left< x_\text{i}^\mu \, x_\text{j}^\mu \right> - C_\text{ij}^μ \left( h_\text{i}^\mu( \mathbf{x}^\mu, ω) \right) \right]^2 \right) \, \text{.}
	\end{split}
\end{align}
In the second line the steady state estimate of the two-point correlation function,
\begin{align}
	\begin{split}
		C_\text{ij}^μ \left( h_\text{i}^\mu( \mathbf{x}^\mu, ω) \right) &\stackrel{\eqref{eqn:exact_relations}}= \frac{1}{2} \frac{ a_\text{i}}{ b_\text{i} + b_\text{j}} \left< \tanh \left( h_\text{i}^\mu( \mathbf{x}^\mu, ω) \right) x_\text{j}^\mu \right> \\ 
		& \quad + \left( \text{i} \leftrightarrow \text{j} \right) + \frac{1}{2} \delta_\text{ij} c_\text{j}^2 \, \text{,}
	\end{split}
\end{align}
is used.
On account of a system-size independent relation between the contributions of first and second moments, we chose a relative factor of the inverse number of nodes, $\nicefrac{1}{N}$.

We additionally use the results of GT developed in section \ref{subsection:Gaussian theory}, $m_\text{i}^\mu( ω)$ and $C_\text{ij}^μ( ω) = χ_\text{ij}^μ( ω) + m_\text{i}^\mu( ω) m_\text{j}^\mu( ω)$, in a least squares approach.
Thus we infer the interaction matrix, $ω_\text{msGt}$, making use of the results of GT for the first two moments,
\begin{align}
	\begin{split}
		ω_\text{msGt} &= \argmin_ω \left( \sum_{ \mu \text{i}} \left[ \left< x_\text{i}^\mu \right> - m_\text{i}^\mu( ω)  \right]^2 + \right. \\
		& \quad \left. + \frac{1}{N} \sum_{ \mu \text{ij}} \left[ \left< x_\text{i}^\mu \, x_\text{j}^\mu \right> - C_\text{ij}^μ( ω) \right]^2 \right) \, \text{.}
	\end{split}
\end{align}

\subsection{Maximum likelihood method} \label{subsection: Maximum likelihood method}

We try to get an unbiased estimate about the model parameter based on gene expression data.
Therefore we employ a maximum likelihood estimate.
The obstacle of our likelihood-based approach to GRN inference is the likelihood calculation.

We quantify the conditional probability, $p( \{ \mathbf{x}^\mu \} | ω, \mathbf{a}, \mathbf{b}, \mathbf{c})$, of measuring a set of gene expression values, $\{ \mathbf{x}^\mu \}$, given a complete set of model parameters.
Therefore, we employ GT to solve the forward problem.
We obtain mean, $\mathbf{m}^\mu( ω)$, and covariance of gene expression, $χ^\mu( ω)$, given the model parameter.
Within our MLM we assume the data to be drawn from a multivariate Gaussian distribution specified by the forward problem results.
This assumption corresponds to the Gaussian model approach introduced in subsection \ref{subsection: Mathematical models}.
Finally we obtain an estimate about the gene regulatory relationships,
\begin{align} \label{eqn:mlGt}
	\begin{split}
		ω_\text{mlGt} &= \argmax_ω\, \left( - \frac{1}{2} \sum_{ \mu \text{k}} \left( \mathbf{x}_\text{k}^\mu - \mathbf{m}^\mu( ω) \right) \chi^\mu( ω)^{-1} \left( \mathbf{x}_\text{k}^\mu - \mathbf{m}^\mu( ω) \right) \right. \\
		& \quad \left. \vphantom{ \sum_{ \mu \text{k}}} - \frac{1}{2} %n_\text{k}
		\ln \det \left( \chi^\mu( ω) \right) \right) \, \text{,}
	\end{split}
\end{align}
by maximisation of the likelihood function, $p( \{ \mathbf{x}^\mu \} | ω, \mathbf{a}, \mathbf{b}, \mathbf{c})$, with respect to $ω$.

\subsection{Comparison of least squares and maximum likelihood method} \label{subsection: Comparison of least squares and maximum likelihood method}

Within this subsection, we compare the results of the least squares methods and our MLM. For the comparison we employ scatter plots of generated and reconstructed interactions as well as the reconstruction error as a quantitative precision measure.
We find that the likelihood-based approach outperforms the other methods in the regime of a strong stochastic contribution to the system dynamics.\\

In this subsection, data is generated by drawing independent random samples from the steady state of our stochastic model of gene regulation.
For inference, we use samples from the steady state of different perturbations, $μ$.

Inference based on our GT is not scalable to large system sizes due to an iterative calculation with numerical integration in each iteration step.
Therefore we focus on the inference of small subnetworks.
Such small subnetworks are realised by regulatory pathways, which are of great importance in the formation and spread of cancer \cite{ Hanahan2000, Hanahan2011}.\\

To visualise the precision of reconstruction, the difference between reconstructed, $ω_{ij}^\text{rec}$, and generated interaction, $ω_{ij}^\text{gen}$, we employ scatter plots.
In figure \ref{plots:inverse_problem_c=1} we show scatter plots of an inferred interaction matrix in a regime of a significant stochastic contribution to system dynamics, $c_\text{i} = 1.0$, based on 50 samples per perturbation.
All four methods tend to overestimate the gene regulatory interactions.
In the case of our MLM, an overestimation is to be expected because of the Gaussian distribution used for the generation of the interaction matrix and the flat prior distribution used in the likelihood-based inference.
In summarising section \ref{section: Likelihood-based gene regulatory network inference}, we discuss the use of prior information in the context of GRNI.
For $c_\text{i} = 1.0$, we find that the likelihood-based approach provides a significantly more accurate reconstruction of the generated interaction matrix compared to the least squares methods.

In the Appendix figure \ref{plots:inverse_problem_c=01} we show scatter plots of an inferred interaction matrix in a regime of a small stochastic contribution to system dynamics, $c_\text{i} = 0.1$.
We find that even with only 5 samples per perturbation the inference in the regime of a small stochastic contribution is significantly more precise.
For $c_\text{i} = 0.1$ wo do not find a significant difference in reconstruction accuracy between the four employed approaches.\\

To quantify the reconstruction precision, we employ a quadratic reconstruction error,
\begin{align} \label{eqn:reconstruction_error}
	r = \sqrt{ \frac{ \sum_{\text{ij}} \left( \omega_\text{ij}^\text{gen} - \omega_\text{ij}^\text{rec} \right) ^ 2}{ \sum_{ \text{ij}} \left( \omega_\text{ij}^\text{gen} \right)^2 }} \, \text{,}
\end{align}
analogously to the forward problem.
The reconstruction error is plotted against the samples per perturbation in figure \ref{plots:reconstruction_error}.
In subplot \ref{plots:reconstruction_error_c=1.0} the inference takes place in a regime of significant stochastic contribution to the system dynamics, $c_\text{i} = 1.0$, whereas in subplot \ref{plots:reconstruction_error_c=0.1} there is only a small stochastic contribution, $c_\text{i} = 0.1$.

As it is to be expected all methods yield a precise reconstruction in the limit of a large number of samples.
The reconstruction is for all four employed methods significantly less precise in the regime of a large stochastic contribution to system dynamics.

Nevertheless, GT within the likelihood-based approach consistently outperforms the other approaches in the regime of a significant stochastic contribution, $c_\text{i} = 1.0$.
With a smaller stochastic contribution, $c_\text{i} = 0.1$, there is no significant difference between the approaches.

In the regime of a large stochastic contribution, $c_\text{i} = 1.0$, we find significant information for the network reconstruction in the second moments of the samples.
This information is visualised in the difference of least squares approach in first- and second-order in figure \ref{plots:reconstruction_error_c=1.0}.
The impact of the probabilistic samples model in the regime of a large stochastic contribution can be estimated by comparing the results of the GT within the likelihood-based approach and the least squares approach based on the GT \ref{plots:reconstruction_error_c=1.0}.
We find that the gain in precision based on the information in second moments and the impact of the probabilistic model in the likelihood function is of the same order of magnitude.

The least square methods in first and second order are only exact in the limit of a large number of samples per perturbation, because they are based on steady state averages.
The maximum likelihood estimate is also valid for a small number of samples per perturbation.
In the limit of only a few samples per perturbation, we expect the likelihood-based approach to outperform the least squares methods.
We do not find such a significant difference as can be seen in figure \ref{plots:reconstruction_error_c=0.1}.
It is not clear why approaches based on the steady state averages work in the limit of only a few samples per steady state without a significant loss of accuracy compared to our MLM.

\begin{figure}
	\centering
	\captionabove[Scatter plot of generated and reconstructed interaction matrix]{Scatter plot of generated and reconstructed interactions for a fully connected system as small as 10 nodes. The model parameter are set to $ a_\text{i} = 1$, $b_\text{i} = 1$, and $ c_\text{i} = 1$. For the inference 10 distinct single drug perturbations with 50 samples per perturbation are used.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright least squares 1st order: $r_\text{ms1o} = 2.13$}
		\includegraphics{chapter/network_inference/graphics/c=1_ω_ms_1o-cropped.pdf}
		\label{plots:c=1_ω_ms_1o-cropped}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright least squares 2nd order: 	$r_\text{ms2o} = 1.81$}
		\includegraphics{chapter/network_inference/graphics/c=1_ω_ms_2o-cropped.pdf}
		\label{plots:c=1_ω_ms_2o-cropped}
	\end{subfigure} \\
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright least squares GT: $r_\text{msGt} = 1.73$}
		\includegraphics{chapter/network_inference/graphics/c=1_ω_ms_gt-cropped.pdf}
		\label{plots:c=1_ω_ms_gt-cropped}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright maximum likelihood GT: 	$r_\text{mlGt} = 1.56$}
		\includegraphics{chapter/network_inference/graphics/c=1_ω_ml_gt-cropped.pdf}
		\label{plots:c=1_ω_ml_gt-cropped}
	\end{subfigure} \label{plots:inverse_problem_c=1}
\end{figure}

\begin{figure}
	\centering
	\captionabove[Reconstruction error plotted against the samples per pertubation]{ The reconstruction error is plotted against the samples per perturbation in a fully connected system as small as 10 nodes and set of model parameter $ a_\text{i} = 1$, $b_\text{i} = 1$, and $c_\text{i} = 0.1$ in subfigure \ref{plots:reconstruction_error_c=0.1} such as $c_\text{i} = 1.0$ in subfigure \ref{plots:reconstruction_error_c=1.0}. For the reconstruction we use 10 distinct single drug perturbations. We employ the least squares fit in first order, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, in second order, \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, and based on the GT, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);. We employ furthermore our MLM, \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright $c_\text{i} = 0.1$}
		\includegraphics{chapter/network_inference/graphics/inference_no_nodes=10_c=01_no_pert=10_no_samples=var_detailed-cropped.pdf}
		\label{plots:reconstruction_error_c=0.1}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright $c_\text{i} = 1.0$}
		\includegraphics{chapter/network_inference/graphics/inference_no_nodes=10_c=1_no_pert=10_no_samples=var-cropped.pdf}
		\label{plots:reconstruction_error_c=1.0}
	\end{subfigure}
	\label{plots:reconstruction_error}
\end{figure}
