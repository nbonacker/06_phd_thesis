
\subsection{Gaussian theory} \label{subsection:Gaussian theory}

In this subsection, we employ a continuous version of the GT developed by Mézard and Sakallariou in the context of the asymmetric Ising model \cite{Mezard2011} to solve the forward problem of the stochastic model of gene regulation.

\subsubsection{GT within the asymmetric Ising model}

The asymmetric Ising model, which we introduced in subsection \ref{subsection:Connection to the asymmetric Ising model}, is characterised by the Hamiltonian
\begin{align} \label{equation:Asymetric Ising Hamiltonian}
\mathcal{H}( \mathbf{s})=-\frac{1}{2} \sum_\text{ij} J_\text{ij} s_\text{i} s_\text{j}-\sum_\text{i} \theta_\text{i} s_\text{i} \, \text{.}
\end{align}
The model parameter are local external fields, $θ_{\text{i}}$, and exchange couplings, $J_\text{ij}$.
To solve the forward problem, Mézard and Sakallariou introduce parallel Glauber dynamics for the asymmetric Ising model, which is outlined in the Appendix section \ref{appendix section: Parallel Glauber dynamics}.
To calculate the local magnetisation and equal-time correlation,
\begin{align}
\begin{split}
m_\text{i} &\stackrel{ \eqref{eqation:exact_relation_first_order_parallel_glauber_dynamics}}{=} \left< \tanh( h_\text{i}) \right> \\
C_\text{ij} &\stackrel{ \eqref{eqation:exact_relation_second_order_parallel_glauber_dynamics}}{=} \left< ( \tanh( h_\text{i}) - m_\text{i}) ( \tanh( h_\text{j}) - m_\text{j}) \right> \, \text{,}
\end{split}
\end{align}
Mézard and Sakallariou approximate the sum over system interaction with spin i, $\sum_{\text{j}} J_\text{ij} s_\text{j}$, with a Gaussian distribution.
Thus the system interaction is characterised by mean,
\begin{align}
\begin{split}
g_\text{i} &= \sum_\text{j} J_\text{ij} m_\text{j} \, \text{, and variance,}\\
\Delta_\text{i} &= \sum_{j} J_\text{ij}^{2}\left(1-m^{2}_\text{j}\right) \, \text{.}
\end{split}	
\end{align}
For the calculation of the variance Mézard and Sakallariou check self-consistently that the typical correlation, $ \left< s_\text{i} s_\text{j} \right> - m_\text{i} m_\text{j}$, is of order $\nicefrac{1}{N}$, where $N$ is the system size.

Within this approximation a set of self consistent equations for $m_\text{i}(t)$ and $C_\text{ij}(t)$,
\iffalse
\begin{align}
\begin{split}
m_\text{i}(t) &= \int \d x \tanh \left( \beta\left(H_\text{i}(t-1)+g_\text{i}(t-1)+x \sqrt{\Delta_\text{i}(t-1)}\right)\right \, \text{and} \\
C_\text{ij}(t)&= \, \text{,}
\end{split}
\end{align}
\fi
which become exact in the limit of a large system size, is derived.
% A solution of the forward problem can than be found by a iterative procedure.
The GT provides a precise solution to the forward problem of the asymmetric Ising model in the regime of strong interactions \cite{Mezard2011}.

\iffalse
\begin{align}
m_\text{i}(t)=\int \d x \tanh \left[\beta\left(H_\text{i}(t-1)+g_\text{i}(t-1)+x \sqrt{\Delta_\text{i}(t-1)}\right)\right]
\end{align}
Thus GT within the asymmetric Ising model relies on the central limit theorem and becomes exact in the limit of many interactions.
\fi

\subsubsection{GT within our stochastic model of gene regulation}

To derive a set of self-consistent equations for $m_\text{i}^\mu$ and $\chi_\text{ij}^\mu$, we assume the local effective field, $h_\text{i}^\mu$, defined within our stochastic model of gene expression in equation \eqref{eqn:definition_external_field} to be Gaussian distributed.
A deterministic contribution, $g_\text{i}^\mu( \mathbf{m}^μ)$, and a probabilistic contribution, $\eta_\text{i}$, to the local effective field,
\begin{align} \label{eqn:gaussian_external_field}
h_\text{i}^\mu( \mathbf{m}^μ) = g_\text{i}^\mu( \mathbf{m}^μ) + \eta_\text{i} \quad \text{with} \quad g_\text{i}^\mu( \mathbf{m}^μ) = \sum_{ \text{k}} \omega_{ \text{ik}} m_\text{k}^\mu + \theta_\text{i} + u_\text{i}^\mu \, \text{,}
\end{align}
are defined.
The probability distribution of $η_\text{i}^μ$ is given by a multivariate normal distribution  $\mathcal{P}( η_\text{i}^μ)$, which is characterised by zero mean and covariance matrix $Δ^μ$.\\

The argument for the Gaussian nature of $h_\text{i}^\mu( \mathbf{m}^μ)$ is that in a sizeable system the local effective field, $h_\text{i}^\mu( \mathbf{m}^μ)$, is the sum of a large number of stochastic variables.
The requirement of uncorrelated variables in the central limit theorem (CLT) only holds in a tree-like graph.
Regarding regulatory motifs like feed forward loops, we can not assume the GRN to be generally tree-like, and we must consider correlated variables.
Under rather technical restrictions, there are versions of the standard CLT that allow for correlations between the random variables that are being summed \cite{ Umarov2008, Hilhorst2009}.
It is unknown whether these restrictions are valid for the effective local field in our stochastic model of gene regulation.
Nevertheless, simulations provide numerical evidence for the Gaussian nature of the local effective field for fully connected systems with as few as ten nodes.\\

To obtain a set of self consistent equations for $m_\text{i}^\mu$ we start with the steady state relation for the mean, $m_\text{i}^\mu =  \nicefrac{ a_\text{i}}{ b_\text{i}} \left< \tanh \left( h_\text{i}^\mu \right) \right>_\text{s} $, which we derived in equation \eqref{eqn:exact_relations}.
Performing the steady state average one obtains the integral equation
\begin{align} \label{eqn:self_consistent_equation_m}
m_\text{i}^\mu &= \frac{1}{\sqrt{2 \pi}} \frac{ a_\text{i}}{ b_\text{i}} \int \text{d} \eta \exp \left( \frac{ \eta^2}{ 2 Δ_\text{ii}^\mu } \right) \tanh \left( g_\text{i}^\mu( \mathbf{m}^μ) + \eta \right) \, \text{.}
\end{align}
To solve this integral equation, one has to calculate the variance of the probabilistic contribution to the external field, $Δ_\text{ii}^\mu$.
Based on the definition of the external field in equation \eqref{eqn:gaussian_external_field}, the relation between covariance of external fields, $Δ_\text{ij}^\mu$, and covariance of gene expression level, $χ_\text{ij}^μ$,
\begin{align} \label{eqn:covariance_external_field}
\begin{split}
Δ_\text{ij}^\mu  = \cov ( η_\text{i}^\mu, η_\text{j}^\mu) &= \cov ( h_\text{i}^\mu, h_\text{j}^\mu) \\ &\stackrel{\eqref{eqn:gaussian_external_field}}{=} \left<  \sum_\text{k} ω_\text{ik} \left( x_\text{k}^μ - m_\text{k}^μ\right) \sum_\text{l} ω_\text{jl} \left( x_\text{l}^μ - m_\text{l}^μ \right) \right>_\text{s} \\
&= \sum_\text{kl} ω_\text{ik} ω_\text{jl} χ_\text{kl}^μ = \left[ ω χ^μ ω^\intercal \right]_\text{ij} \, \text{,}
\end{split}
\end{align}
is calculated.
Equipped with the equations \eqref{eqn:self_consistent_equation_m} and \eqref{eqn:covariance_external_field} it is left over to find an expression for $χ_\text{ij}^μ$ to solfe the forward problem within the Gaussian approximation.
To derive also a set of self consistent equations for $χ_\text{ij}^μ$, we calculate the steady state averages $\left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu \right>_\text{s}$. % in the definition $ \left< x_\text{i}^\mu x_\text{j}^\mu \right>_\text{s} = \nicefrac{ a_\text{i}}{ ( b_\text{i} + b_\text{j})} \left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu \right>_\text{s} + \left( \text{i} \leftrightarrow \text{j} \right) + \nicefrac{c_\text{i}^2}{ ( b_\text{i} + b_\text{j})} \delta_\text{ij}$, which is introduced in \eqref{eqn:exact_relations}.
Therefore the interaction matrix, 
\begin{align} \label{eqn:expression_levels_through_external_fields}
x_\text{i}^\mu = \sum_\text{k} ω_\text{ik}^{-1} ( h_\text{k}^\mu - \theta_\text{k} - u_\text{k}^\mu) \, \text{,}
\end{align}
is inverted to replace the expression levels, $x_\text{i}^\mu$, with the external fields, $ h_\text{k}^\mu$.
With the inverted equation $\eqref{eqn:expression_levels_through_external_fields}$ one can rewrite the steady state average,
\begin{align} \label{eqn:state_state_average_tanh_h_x}
\left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu \right>_\text{s} \stackrel{ \eqref{eqn:expression_levels_through_external_fields}}{=} \sum_\text{k} ω_\text{ik}^{-1} \left< \left( h_\text{k}^\mu - u_\text{k}^μ \right) \tanh \left(  h_\text{j}^\mu \right) \right>_\text{s} \, \text{,}
\end{align}
as a sum over steady state averages containing only external fields, which we assumed to be Gaussian.
To calculate the steady state averages in \eqref{eqn:state_state_average_tanh_h_x} the correlation coefficient,
\begin{align}
ρ_\text{jk} = \frac{ \cov ( h_\text{j}^\mu, h_\text{k}^\mu)}{ \sqrt{ Δ_\text{jj}^μ Δ_\text{kk}^μ}}  \, \text{,}
\end{align}
is defined. 
The multivariate normal distribution of the external field is expanded in linear order in $ρ_\text{jk}$,
\begin{align} \label{eqn:expanded_probability_distribution}
\mathcal{P}( η_\text{j}^μ, η_\text{k}^μ) = \mathcal{P}( η_\text{j}^μ) \mathcal{P}( η_\text{k}^μ) \left( 1 + \frac{η^μ_\text{j}}{ \sqrt{ Δ_\text{jj}^μ}} \frac{η^μ_\text{k}}{ \sqrt{ Δ_\text{kk}^μ}} ρ_\text{jk} \right) + σ(ρ_\text{jk}^2).
\end{align}
Using the expanded probability distribution \eqref{eqn:expanded_probability_distribution}, an expression for the steady state averages on the right hand side of $\eqref{eqn:state_state_average_tanh_h_x}$,
\begin{align} \label{eqn:steady_state_average_tanh_h_h}
\left< h_\text{k}^\mu \tanh \left(  h_\text{j}^\mu \right) \right>_\text{s} \stackrel{\eqref{eqn:expanded_probability_distribution}}{=} g_\text{k}^μ m_\text{j}^μ + λ_\text{j}^μ \cov (h_{k}^μ h_{j}^μ) \text{,}
\end{align}
is calculated.
The factor $\lambda_\text{j}^\mu$, which is a measure for sensitivity of the first moments to fluctuations in the expression levels, is given by the integral equation
\begin{align} \label{eqn:integral_equation_λ}
\lambda_\text{j}^\mu &= \frac{1}{\sqrt{2 \pi}} \int \text{d} \eta \exp \left( \frac{ \eta^2}{ 2 \Delta_\text{ii}^\mu } \right) \left( 1 - \tanh^2 \left( g_\text{j}^\mu + \eta \right) \right) \, \text{.}
%\\ \Delta_\text{i}^\mu &= \left( \omega  \chi^\mu \omega ^\text{T} \right)_\text{ii}
\end{align}
Using the definition of $χ_\text{ij}^μ$ in equation \eqref{eqn:definition_first_and_second_moment},
the steady state averages \eqref{eqn:state_state_average_tanh_h_x} and \eqref{eqn:steady_state_average_tanh_h_h}, one gets an expression for the covariance
\begin{align}
\begin{split}
χ_\text{ij}^μ &\stackrel{\eqref{eqn:definition_first_and_second_moment}}{=} \frac{ a_\text{i}}{ b_\text{i} + b_\text{j}} \left< \tanh \left( h_\text{i}^\mu \right) x_\text{j}^\mu \right>_\text{s} + \left( \text{i} \leftrightarrow \text{j} \right) + \frac{c_\text{i}^2}{  2b_\text{i}} \delta_\text{ij} - \left< x_\text{i}^\mu \right> \left< x_\text{j}^\mu \right> \\ &\stackrel{ \eqref{eqn:state_state_average_tanh_h_x} \eqref{eqn:steady_state_average_tanh_h_h}}{=} \frac{ a_\text{i}}{ b_\text{i} + b_\text{j}} \sum_\text{k} ω_\text{ik}^{-1} \left( g_\text{k}^μ m_\text{j}^μ + λ_\text{j}^μ \cov (h_{k}^μ h_{j}^μ) - u_\text{k}^μ m_\text{j}^μ \right) \\ &\quad+ \left( \text{i} \leftrightarrow \text{j} \right)  + \frac{c_\text{i}^2}{2 b_\text{i}} - m_\text{i}^μ m_\text{j}^μ \, \text{.}
\end{split}
\end{align}
With the definition of $g_\text{i}^\mu( \mathbf{m}^μ)$ in equation \eqref{eqn:gaussian_external_field} and the covariance of the external field in equation \eqref{eqn:covariance_external_field} 
%\begin{align}
%	g_\text{i}^\mu - u_\text{i}^\mu = \sum_{ \text{k}} \omega_{ \text{ik}} m_\text{k}^\mu + \theta_\text{i} \quad \text{and} \quad \cov ( h_\text{i}^\mu, h_\text{j}^\mu) = \left[ ω χ ω^\intercal \right]_\text{ij}
%\end{align}
one finally obtains a set of self-consistent equations for the covariance matrix of the expression level, 
\begin{align} \label{eqn:self_consistent_equation_χ}
\chi_\text{ij}^\mu &= \frac{a_\text{j}}{ b_\text{i} + b_\text{j}} \left( \chi^\mu \omega ^\text{T} \right)_\text{ij} \lambda_\text{j}^\mu + \left( \text{i} \leftrightarrow \text{j} \right) + \frac{c_\text{i}^2}{2 b_\text{i}} \delta_\text{ij} \, \text{.}
\end{align}
To obtain a solution of the forward problem within GT we solve iteratively the set of self-consistent equations \eqref{eqn:self_consistent_equation_m} and \eqref{eqn:self_consistent_equation_χ}.
In each iteration step the integrals in \eqref{eqn:self_consistent_equation_m} and in \eqref{eqn:integral_equation_λ} are solved numerically using adaptive Gauss-Kronrod quadrature \cite{Laurie1997}.
We find that a simple iterative procedure with initial values $\mathbf{m}^μ_\text{init} = \mathbf{0}$ and $χ^μ_\text{init} = \mathbb{1}$ converges.
Finally, this is our solution to the forward problem within the mean field approximation.
