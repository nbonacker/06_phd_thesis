
\chapter{Conclusion}

\epigraph{Science never solves a problem without creating ten more.}{George Bernard Shaw}

Within this chapter, we conclude our research, combining statistical mechanics and information theory with high-throughput technologies, to address open questions in systems biology.

In section \ref{section: Likelihood-based gene regulatory network inference}, we summarise our findings on network inference and biological information processing.
We review our results on the frameshift-based prediction of CBI response in section \ref{Frameshift-based cancer immunotherapy response prediction}.

\section{Likelihood-based gene regulatory network inference} \label{section: Likelihood-based gene regulatory network inference}

Our proposed system of stochastic differential equations based on an interaction network \eqref{eqn:stochastic_differential_equations} proves to be a promising model for the research on biological information processing.
We employ our dynamic model to construct a maximum likelihood estimate for the interaction network based on gene expression data.

Based on exact steady state relations \eqref{eqn:exact_relations} we can solve the forward problem within the MFT and our GT.
We find that the GT outperforms MFT in the regime of strong inter-gene couplings, where the mean field approximation breaks down.

Based on the GT, we propose an MLM to solve the inverse problem.
In the regime of a sizeable stochastic contribution to the system dynamics, we find that our MLM outperforms standard least squares fits, which are successfully used for the reconstruction of GRNs \cite{ Molinelli2013, Korkut2015}.
Within our MLM, we implicitly assume a flat prior distribution, which causes an overestimation of interactions.
We find this overestimation also within the least squares methods.
In the case of the GT, the use of a prior distribution can solve this straightforwardly.
Whereas in the case of the least squares methods, one needs an additional term in the cost function that penalises the network complexity \cite{Molinelli2013}.

We can predict unknown gene expression levels in the SK-MEL-133 cell line with all investigated methods.
We show that the approaches based on the GT yield a significantly smaller mean squared error than the least squares methods.

We find that the inferred regulatory interactions do not represent known regulatory relationships.
This issue constitutes a lack of interpretability because we constructed an accurate prediction system with hidden internal logic.\\

A decisive difference between simulated and experimental perturbation data is the missing availability and specificity of drugs, such that one can not systematically perturb all genes within an experiment.
Even with sufficient drugs, one is limited by the financial and temporal cost of testing many drug combinations with multiple biological replicates per perturbation.
Moreover, in perturbation experiments, the number of perturbations is typically as large as the system size with only a few samples per perturbation \cite{ Nelander2008, Molinelli2013, Korkut2015}.

A high ratio between the number of genes and the sample size makes the inference of biological knowledge without integrating prior knowledge infeasible.
Therefore, taking present-day data availability into account, biological prior information that may be incorrect in a particular physical context is crucial for GRNI.
We note that quantitative GRNI is usually based on information from the literature or interaction databases \cite{Phillip2004, Ghanbari2015, Altarawy2017}.\\

Our MLM should be helpful in open questions about gene regulation and biological information processing. 
One can extend our approach into several future research directions.

Research literature and online databases offer a vast amount of biological knowledge about gene regulation and signalling networks.
The integration of biological knowledge is challenging due to programmatic access, various gene name conventions, and quantification of prior information. 
Nonetheless, our MLM offers a straightforward way to incorporate prior knowledge to achieve reliable network reconstruction.

The investigation of the posterior landscape could be another promising starting point for further research.
Based on the posterior distribution, one could quantify the uncertainty of inferred regulatory relationships and propose promising drug combinations to test uncertain interactions.

We used protein concentration and phosphorylation data from a cell-line perturbation experiment.
However, incorporating additional information on mRNA concentration could be a starting point for the inference of multi-level gene regulation.
By combining mRNA and protein data, one can expect to generate whole-cell models of gene regulation and signalling pathways that support the design of clinical trials.

Finally, the investigation of hidden nodes within the GRN could be a future research direction.
We base our network inference on the convenient but unrealistic hypothesis that all the relevant gene expression levels are measured.
In practice, it is impossible to be sure that there are no other interacting genes.
At the cost of a larger parameter space, one could extend our MLM by allowing some hidden nodes.

\section{Frameshift-based cancer immunotherapy response prediction} \label{Frameshift-based cancer immunotherapy response prediction}

We address the potential of statistical inference for clinical decision-making in the context of cancer immunotherapy.
Our statistical analysis of CBI data revealed that the number of frameshift mutations is not significantly associated with response to CBI.
Nonetheless, we find slight evidence that frameshift mutations are related to CBI response.
Our findings are compatible with a hidden factor, e.g. the mutation rate, that increases both the number of unknown immunogenic mutations and the number of frameshifts.
Still, there is no evidence that the frameshift mutations are causal for CBI response.

We found that cross-validation is essential within the investigation of hypothetical determinants to CBI response.
Free parameters tend to improve the descriptiveness at the cost of predictiveness.
Based on noisy data within a small dataset, even a statistical model with few model parameters is prone to overfitting.
Such a model will probably describe a data set well but will usually fail in making predictions. 
In addition to the model parameter selection, this also applies to a subset choice based on cancer subspecies or other clinical factors.
There is evidence that confounding of cancer subtypes and incorrect statistical tests lead to previously reported response determinants \cite{Mirny2020}.

During the timespan of our research on CBI response prediction, a promising marker for melanoma is found.
The protein midkine (MDK) is a driver of an inflamed, but immune-evasive tumour microenvironment that is correlated with resistance to CBI in melanoma patients \cite{Soengas2020}.
The study \cite{Soengas2020} links MDK expression with poor CBI outcome and points out a promising combined MDK immune checkpoint inhibition.
Moreover, the study stresses the importance of a systemic investigation of CBI response.

The understanding of molecular and cellular drivers of immune escape is one of the biggest challenges to move the field of cancer immunotherapy forward \cite{Hedge2020}.
The identification of immunogenic mutations is an open research question.
Extensive CBI trials and statistical inference can provide knowledge about predictive biomarkers to improve clinical decision-making.
