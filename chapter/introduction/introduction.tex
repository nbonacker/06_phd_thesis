
\chapter{Introduction} \label{chapter: Introduction}

\epigraph{The job of the first eight pages is not to have the reader want to throw the book at the wall, during the first eight pages.}{David Foster Wallace}

Systems biology is an interdisciplinary field of research that studies complex interactions within biological systems.
This thesis combines computational methods from broadly varying research fields such as information theory, computational biology, and statistical mechanics.
We unify these methods to learn about complex biological systems based on an interplay between information, probability, and logic.

We address two biological problems within this thesis.
In section \ref{Gene regulatory network inference}, we introduce the inference of gene regulatory networks.
The second problem, the response prediction to cancer immunotherapy, is posed in section \ref{Cancer immunotherapy response prediction}.

\section{Gene regulatory network inference} \label{Gene regulatory network inference}

The regulation of gene expression is fundamental in the complexity and diversity of life.
Gene regulation controls everything from the response of unicellular organisms to environmental changes up to cell differentiation and self-organisation in multicellular organisms.
The multitude of genes forms a complex gene regulatory network (GRN).
The GRN encodes response patterns, cell differentiation, and self-organisation.
Obtaining reliable information about gene regulation is essential to understanding the complexity and diversity of biological organisms.

Anomalous gene regulation plays a crucial role in the formation and spread of cancer.
Therefore, knowledge about GRNs in increasing levels of complexity, from small signalling cascades to large gene regulatory networks consisting of thousands of genes, is important in cancer biology and designing new targeted therapies against cancer.
Mathematical modelling of gene regulation, inference of regulatory interactions, and prediction of gene expression is a promising scientific issue in the field of cancer research \cite{Hecker2009}.\\

We use a stochastic model of gene expression dynamics based on an interaction network to infer regulatory relationships from gene expression data.

Gene expression is, due to the small number of molecules involved and randomness in transcription and translation, an intrinsic stochastic process \cite{Raj2008}.
We investigate whether one can learn about regulatory interactions from correlations between fluctuations in gene expression.
To this end, we propose a system of stochastic differential equations to model gene expression and employ stochastic calculus to characterise the steady state distribution of our system.

Based on the steady state characterisation, we solve the forward problem, calculating mean gene expression and their covariance given a set of model parameters.
For this purpose, we extend approximation methods developed in statistical physics and artificial neural network research.

Gene regulatory network inference (GRNI), the reconstruction of regulatory interactions given gene expression measurements, is an inverse problem.
For the solution of this inverse problem, we employ statistical inference and construct a maximum posterior estimate based on our forward problem solution.

We address the question of whether our maximum posterior estimate yields a more accurate network reconstruction than standard methods based on a least squares fit \cite{ Nelander2008}.
We reconstruct networks based on simulated data and predict gene expression within a cell line experiment to compare the approaches.\\

The reconstruction of a GRN based on gene expression data is a computationally challenging problem.
Without assumptions on the network structure, the computation of a maximum posterior estimate for the structure in an undirected regulatory model is an NP-hard problem \cite{Chandrasekaran2012}.
Thus, undirected network inference is at least as hard as the hardest problems in the set of problems solvable in polynomial time.
The number of possible configurations for a model with $n$ nodes and $d$ directed interactions is $d^{n^{2}}$.
Accordingly, the combinatorial explosion of configurations is a computational challenge \cite{Klamt2002}.
Due to the computational complexity, we focus on small regulatory networks to provide proof of principle.

On account of experimental limitations, quantifying gene expression in single-cell high-throughput experiments usually incurs cell destruction.
Therefore we focus on the inference without time-series data, which is typical in high-throughput experiments in the context of GRNI.
Because the inverse problem of inference from a single-time point without time-series data is ambiguous, perturbation experiments are used in the field of systems biology to infer causality in GRNs.
The idea of a perturbation experiment is to quantify steady state gene expression without any perturbation and then apply a known combination of drugs to the system and measure the gene expression after settling into the perturbed steady state.
On account of systematic perturbations, more information is available to determine regulatory mechanisms.
The approach of perturbation experiments has been successfully employed to infer GRNs \cite{ Korkut2015}.

\section{Cancer immunotherapy response prediction} \label{Cancer immunotherapy response prediction}

Immune checkpoints play a crucial role in the regulation of the immune system.
Genetic mutations occur that affect these immune checkpoints and disable the immune system's ability to recognise and destroy tumour cells during the development of many cancers. 
Checkpoint blockade immunotherapy (CBI), which inhibits these immune checkpoints, enables an immune response again.

For a minority of cancer patients, checkpoint inhibition has an outstanding clinical benefit \cite{Ribas2018}.
Nonetheless, adverse effects and high variability in patient response limits clinical success.
The understanding of determinants to CBI response is a key scientific issue in the field of immuno-oncology \cite{Havel2019}.

The obstacle in predicting response to CBI is the complex behaviour of the immune system in cancer \cite{Weinberg2013, Sompayrac2019}.
Due to a large number of hypothetical decisive response determinants, careful analysis is required.

The focus of CBI response prediction based on genetic alterations has been mainly on point mutations.
The contribution of frameshift mutations is less well studied.
Swanton et al. find that the number of frameshift mutations is significantly associated with CBI response across three melanoma cohorts \cite{Swanton2017}.

We focus on frameshift-derived peptide sequences that are entirely different from self-peptides.
These frameshift-derived peptides are a hypothetical rich source of immunogenic targets.
We investigate whether one can predict cancer immunotherapy response based on frameshift mutations within the tumour genome.
To answer this question, the information content of hypothetical frameshift-related response determinants is analysed.
We use tools from computational biology to process patient-specific mutation data.
To investigate information about CBI response within frameshift mutations in the tumour genome, we implement statistical inference.
We aim to identify patients that likely benefit from CBI and support clinical decision-making.