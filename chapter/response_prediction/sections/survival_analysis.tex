
\section{Survival analysis} \label{section:Survival analysis}

We employ survival analysis to investigate hypothetical determinants of overall survival after the start of CBI.
Analogously to the previous section, we explore the predictive power of hypothetical survival determinants in subsection \ref{subsection: Hypothetical survival determinants} and try to predict the overall survival in subsection {subsection: Survival prediction}.

\subsection{Hypothetical survival determinants} \label{subsection: Hypothetical survival determinants}

To quantify the predictive power of hypothetical survival determinants, we divide the set of patients according to a real-valued score function, representing a hypothetical response determinant, into a high-score and a low-score subset.
Within our datasets, we find that about 30\% of the patients are classified as responders.
We use this prior knowledge for the subset size.
We calculate Kaplan-Meier estimators and visualise the estimated survival rates in a Kaplan-Meier plot.

We explore the predictive value of different mutation types within a lung cancer and melanoma dataset in the figures \ref{figure:Number_Mutation_Lung_Miao} and \ref{figure:Number_Mutation_Melanoma_Miao}.
We show the corresponding Kaplan-Meier curves for a second melanoma dataset in the appended figure \ref{figure:Number_Mutation_Melanoma_Liu}.

We find that the patient group with more frameshift mutations has a higher survival rate within the Miao lung cancer data.
There is no significant difference in overall survival after the start of CBI between the high-score and the low-score subset in the Miao melanoma data.
It is unclear whether the number of frameshift mutations is the cause of CBI response or whether there is a common cause for the frameshift mutation and the overall survival.
In contrast to the binary response prediction, we do not find a response signal of silent mutations in overall survival within the lung cancer dataset.

We investigate whether the number of frameshift mutations has a predictive value apart from the overall number of mutations.
We, therefore, divide the whole dataset into two subsets of equal size according to the overall mutation number and divide each of these subsets again into two subsets of equal size according to the frameshift mutation number.
The corresponding ROC curves are shown in figure \ref{figure:Matrix_Miao2018} and for the second melanoma dataset in the Appendix figure \ref{figure:Matrix_Liu2019}.
Within the lung cancer dataset, we find slight evidence that the number of frameshift mutations in the subset characterised by a high overall number of mutations has some predictive value.
The melanoma datasets also support this minor trend.

\begin{figure}
	\centering
	\captionabove[Kaplan-Meier plot]{Kaplan-Meier curves based on hypothetical survival determinants for lung cancer patients within the Miao dataset \cite{Miao2018}, high score subset, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);, low score subset, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Missense mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Missense_Mutation_Lung_Miao-cropped.pdf}
		\label{subfigure:Number_Missense_Mutation_Lung_Miao}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Nonsense mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Nonsense_Mutation_Lung_Miao-cropped.pdf}
		\label{subfigure:Number_Nonsense_Mutation_Lung_Miao}
	\end{subfigure} \\
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Frameshift mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Frameshift_Mutation_Lung_Miao-cropped.pdf}
		\label{subfigure:Number_Frameshift_Mutation_Lung_Miao}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Silent mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Silent_Mutation_Lung_Miao-cropped.pdf}
		\label{subfigure:Number_Silent_Mutation_Lung_Miao}
	\end{subfigure}
	\label{figure:Number_Mutation_Lung_Miao}
\end{figure}

\begin{figure}
	\centering
	\captionabove[Kaplan-Meier plot]{Kaplan-Meier curves based on hypothetical survival determinants for melanoma patients within the Miao dataset \cite{Miao2018}, high score subset, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);, low score subset, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright Missense mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Missense_Mutation_Melanoma_Miao-cropped.pdf}
		\label{subfigure:Number_Missense_Mutation_Melanoma_Miao}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright Nonsense mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Nonsense_Mutation_Melanoma_Miao-cropped.pdf}
		\label{subfigure:Number_Nonsense_Mutation_Melanoma_Miao}
	\end{subfigure} \\
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright Frameshift mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Frameshift_Mutation_Melanoma_Miao-cropped.pdf}
		\label{subfigure:Number_Frameshift_Mutation_Melanoma_Miao}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright Silent mutation}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/Number_Silent_Mutation_Melanoma_Miao-cropped.pdf}
		\label{subfigure:Number_Silent_Mutation_Melanoma_Miao}
	\end{subfigure}
	\label{figure:Number_Mutation_Melanoma_Miao}
\end{figure}

\begin{figure}
	\centering
	\captionabove[]{Kaplan-Meier curves for patient groups characterised by high overall and high frameshift, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, high overall and low frameshift, \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, low overall and high frameshift, \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);, low overall and low frameshift, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex); number of mutation.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Lung - Miao}
		\label{subfigure:Matrix_Lung_Miao2018}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Melanoma - Miao}
		\label{subfigure:Matrix_Melanoma_Miao2018}
	\end{subfigure} \\
	\begin{subfigure} [b] { 0.48\textwidth}
		\centering
		\includegraphics{chapter/response_prediction/graphics/Matrix_Lung_Miao2018-cropped.pdf}
	\end{subfigure}
	\begin{subfigure} [b] { 0.48\textwidth}
		\centering
		\includegraphics{chapter/response_prediction/graphics/Matrix_Melanoma_Miao2018-cropped.pdf}
	\end{subfigure}
	\label{figure:Matrix_Miao2018}
\end{figure}

\FloatBarrier

\subsection{Survival prediction} \label{subsection: Survival prediction}

We try to predict the overall survival after the start of CBI within the lung cancer dataset analogously to the binary response prediction.
For this purpose, we employ our stochastic survival model defined in subsection \ref{subsection: Statistical classification} to divide the set of patients into a high-score and a low-score subset.
We based our prediction on the number of missense and frameshift mutations.

The posterior distribution of the model parameter is characterised within subfigure \ref{subfigure:scatter_linear_regression_miao_lung_3}.
Based on our MAP estimate, we employ our stochastic survival model to predict overall survival.
We use the same cross-validation scheme as in our binary response prediction.
The resulting Kaplan-Meier curves, which are based on our MAP estimate, are plotted in subfigure \ref{subfigure:KaplanMeier_Linear_Regression}.
We find that the high-score patient group has a higher survival rate than the low-score group.  
Notwithstanding, this prediction is not significantly better than a prediction solely based on the number of frameshift mutations.

\begin{figure}
	\centering
	\captionabove[]{We employ Bayesian linear regression for CBI survival prediction based on the number of missense and frameshift mutations.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Samples from the posterior, \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, 
			MAP estimate, \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);, 1$σ$ credible region, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);, 2$σ$ credible region, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);.}
		\label{subfigure:scatter_linear_regression_miao_lung_3}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Kaplan-Meier curve based on a MAP survival prediction within our linear model.}
		\label{subfigure:KaplanMeier_Linear_Regression}
	\end{subfigure} \\
	\begin{subfigure} [b] { 0.48\textwidth}
		\centering
		%\caption{ Scatter plot.}
		\includegraphics[width=\textwidth]{chapter/response_prediction/graphics/survival_analysis/scatter_linear_regression_miao_lung_3-cropped.pdf}
		%\label{subfigure:scatter_logistic_regression_miao_lung_3}
	\end{subfigure}
	\begin{subfigure} [b] { 0.48\textwidth}
		\centering
		%\caption{ ROC curve.}
		\includegraphics{chapter/response_prediction/graphics/survival_analysis/KaplanMeier_Linear_Regression-cropped.pdf}
		%\label{subfigure:ROC_Curve_Logistic_Regression}
	\end{subfigure}
	\label{figure:linear_regression_miao_lung}
\end{figure}
