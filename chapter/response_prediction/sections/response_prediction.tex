
\section{Statistical classification} \label{section:Statistical classification}

We employ binary classification to investigate the information content in hypothetical CBI response determinants.
For this purpose, we map the discrete response evaluation criteria in solid tumours (RECIST) to a binary outcome variable, response and no-response.
The mapping is shown in table \ref{table:RECIST to a binary outcome variable}.

In this and the following section, we analyse datasets published by Miao et al. \cite{Miao2018} and Liu et al. \cite{Liu2019}.
The Miao dataset is based on whole-exome sequencing of 249 tumours and healthy tissue from patients with clinically evaluated responses to CBI across multiple cancer types.
The Liu dataset contains a cohort of 144 melanoma patients treated with anti-PD1 CBI.
These datasets contain patient-specific information about genetic mutations, clinical information about therapy's effects, and the overall survival after the start of CBI.
The information about various types of genetic mutations are based on variant calling, the identification of mutations from sequencing data \cite{Nielsen2011}.
We chose these datasets because of the relatively large number of patients, their public accessibility, and the included information about frameshift mutations.

Within the first subsection of this section, we use the ROC to investigate hypothetical response determinants.
To predict the outcome of CBI, we employ the framework of Bayesian logistic regression within the second subsection.

\begin{table}
	\begin{center}
		\caption{Map from RECIST to a binary outcome variable}
		\label{table:RECIST to a binary outcome variable}
		\begin{tabular}{c|c|c}
			& Miao & Liu \\ \hline
			response & clinical benefit & \makecell{complete response \& \\ partial response} \\ \hline
			no-response    & \makecell{stable disease \& \\ no clinical benefit} & \makecell{stable disease \& \\  progressive disease}
		\end{tabular}
	\end{center}
\end{table}

\subsection{Hypothetical response determinants} \label{subsection: Hypothetical response determinants}

Given the binary response, we compare the predictive value of frameshift mutations with other mutation types.
Therefore, we study the number of mutations as a classifier within a series of ROC curves, which is depicted in figure \ref{figure:ROC_Curve_Number_Mutation_I_Miao}.
We do not find a significant difference in the predictive value between the number of missense, nonsense, frameshift, and silent mutations.
This finding is also valid within a second melanoma dataset in the Appendix subfigure \ref{figure:ROC_Curve_Number_Mutation_I_Melanoma_Liu}.
In comparison between cancer types, we find that the number of mutations has a higher predictive value for patients with lung cancer than melanoma patients.

All ROCs in figure \ref{figure:ROC_Curve_Number_Mutation_I_Miao} are comparable and are in particular similar with silent and nonsense mutations.
Thus, the data are compatible with a hidden factor, e.g. the mutation rate, which increases the number of all mutations independent of whether they are a hypothetical target for an immune response.
The silent and nonsense mutations are not immunogenic, but they still have a signal via this implicit mechanism.

In the second series of ROC curves, we investigate hypothetical response determinants related to frameshift mutations, which we defined in section \ref{section: Hypothetical frameshift-based response determinants}.
The ROC curves are shown in figure \ref{figure:ROC_Curve_Frameshift_Mutation_Peptides}.
In addition to the underlying signal of the number of frameshift mutations, we do not find an increased predictive value within the investigated frameshift-related response determinants.
We also find no response signal in the second melanoma data set, shown in the attached subfigure \ref{figure:ROC_Curve_Frameshift_Mutation_Melanoma_Liu}.

\begin{figure}
	\centering
	\captionabove[ ROC curve based on the number of a specific mutation type as classifier.]{ ROC curve based on the number of a specific mutation type as classifier. We examine the number of missense \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, nonsense \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, frameshift \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);, and silent mutation \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Lung - Miao}
		\includegraphics{chapter/response_prediction/graphics/response_prediction/ROC_Curve_Number_Mutation_I_Lung_Miao-cropped.pdf}
		\label{subfigure:ROC_Curve_Number_Mutation_I_Lung_Miao}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright Melanoma - Miao}
		\includegraphics{chapter/response_prediction/graphics/response_prediction/ROC_Curve_Number_Mutation_I_Melanoma_Miao-cropped.pdf}
		\label{subfigure:ROC_Curve_Number_Mutation_I_Melanoma_Miao}
	\end{subfigure}
	\label{figure:ROC_Curve_Number_Mutation_I_Miao}
\end{figure}

\begin{figure}
	\centering
	\captionabove[ ROC curve for frameshift mutation based classifier.]{ 
		ROC curve for frameshift mutation based classifier.
		We examine the number of frameshift-derived peptides, \tikz \draw[black,fill=black] (0,0) circle (0.7ex);, the accumulated frequency \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, the accumulated expression \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, the length of frameshift  peptides \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);, and number of MHC-binding frameshift-derived peptides \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Lung - Miao}
		\includegraphics{chapter/response_prediction/graphics/response_prediction/ROC_Curve_Frameshift_Mutation_Peptides_Lung_Miao-cropped.pdf}
		\label{subfigure:ROC_Curve_Frameshift_Mutation_Peptides_Lung_Miao}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ \protect\raggedright Melanoma - Miao}
		\includegraphics{chapter/response_prediction/graphics/response_prediction/ROC_Curve_Frameshift_Mutation_Peptides_Melanoma_Miao-cropped.pdf}
		\label{subfigure:ROC_Curve_Frameshift_Mutation_Peptides_Melanoma_Miao}
	\end{subfigure}
	\label{figure:ROC_Curve_Frameshift_Mutation_Peptides}
\end{figure}

\subsection{Response prediction} \label{subsection: Response prediction}

We use statistical inference to make a statement about the significance of the classification based on the response determinants studied.
For this purpose, we employ our stochastic response model defined in subsection \ref{subsection: Statistical classification}.
We focus on the number of missense and frameshift mutations as input variables for our binary response prediction.
Within the lung cancer dataset, we found a small response signal.
Therefore we focus on the response prediction within this dataset.

In subfigure \ref{subfigure:scatter_logistic_regression_miao_lung_3} we show a scatter plot of the drawn samples for the regression coefficients, $β_1$ and $β_2$, corresponding to the missense and frameshift mutations.
The number of missense mutations is generally much larger.
Thus the axis scaling is not comparable.

We do not find a significant response signal.
The null hypothesis, $\forall i : β_\text{i} = 0$, is within the 1$σ$ credible region. 
Therefore, there is no evidence at the 1$σ$ level that CBI response depends on the number of missense and frameshift mutations.

We employ our stochastic response model to predict binary CBI response based on our MAP estimate of the model parameter.
For this purpose, we employ cross-validation.
We divide the data into two sets, one prediction set, consisting of three individuals, and a training set, with the other 54 patients.
The response score for each patient relies on a maximum likelihood estimate based on the corresponding training set.
The resulting cross-validated ROC curve is plotted in subfigure \ref{subfigure:ROC_Curve_Logistic_Regression}.
A comparison with subfigure \ref{subfigure:ROC_Curve_Number_Mutation_I_Lung_Miao} shows that the prediction based on our stochastic response model is not significantly better than a classification based on the number of frameshift mutations.
This finding is compatible with the hypothesis that a generally high mutation right increases both the number of unknown immunogenic mutations and the number of frameshifts.
Still, there is no evidence that the frameshift mutations are causal for CBI response.

\begin{figure}
	\centering
	\captionabove{We employ Bayesian logistic regression for CBI response prediction based on the number of missense and frameshift mutations.}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Samples from the posterior, \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, 
			MAP estimate, \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);, 1$σ$ credible region, \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);, 2$σ$ credible region, \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);.}
		\label{subfigure:scatter_logistic_regression_miao_lung_3}
	\end{subfigure}
	\begin{subfigure} [t] { 0.48\textwidth}
		\centering
		\caption{ Cross-validated ROC curve based on a MAP estimate within our stochastic model.}
		\label{subfigure:ROC_Curve_Logistic_Regression}
	\end{subfigure} \\
	\begin{subfigure} [b] { 0.48\textwidth}
		\centering
		%\caption{ Scatter plot.}
		\includegraphics[width=\textwidth]{chapter/response_prediction/graphics/response_prediction/scatter_logistic_regression_miao_lung_3-cropped.pdf}
		%\label{subfigure:scatter_logistic_regression_miao_lung_3}
	\end{subfigure}
	\begin{subfigure} [b] { 0.48\textwidth}
		\centering
		%\caption{ ROC curve.}
		\includegraphics{chapter/response_prediction/graphics/response_prediction/ROC_Curve_Logistic_Regression-cropped.pdf}
		%\label{subfigure:ROC_Curve_Logistic_Regression}
	\end{subfigure}
	\label{figure:logistic_regression_miao_lung}
\end{figure}
