
\section{Mathematical foundations of statistical classification and survival analysis} \label{section:Mathematical foundations of statistical classification and survival analysis}

We base our CBI response prediction on information about genetic alterations within the patient's genome.
For this purpose, we employ statistical classification and survival analysis.

In subsection \ref{subsection: Statistical classification}, we introduce the framework of statistical classification.
We employ statistical classification in the following section to build a stochastic model predicting binary CBI response.
We try to divide the set of patients into responders and non-responders based on information about their genetic alterations.

In subsection \ref{subsection: Survival analysis}, we introduce the framework of survival analysis, which is a branch of statistics for the analysis of time-to-event data, such as the overall survival after the start of therapy.

\subsection{Statistical classification} \label{subsection: Statistical classification}

Before we introduce the method of Bayesian logistic regression,
we formulate the statistical classification problem for a binary output variable.
The classification problem and the method of Bayesian logistic regression are introduced in Bishop's excellent textbook "Pattern Recognition and Machine Learning" \cite{Bishop2006}.
To quantify the performance of a classifier, we define a quality measure in the closing paragraph.

\subsubsection{Binary classification problem}

Binary classification in the context of our CBI response prediction is the task of classifying patients into responders and non-responders based on tumour genetic information.
Cancer tissue samples are successfully classified by their gene expression patterns for clinical decision-making \cite{Tan2005}.

Within our CBI response prediction, we base the classification of a patient on an input variable, $\mathbf{ x}^\text{n}$, with patient index $n$, and a classification rule.
The $\mathbf{ x}^\text{n}$ characterise the mutational landscape of a patient with a real-valued vector.
The classification rule is a mapping to a binary output variable, $\mathbf{ x}^\text{n} \mapsto k^\text{n}$ with $ k^\text{n} \in \left\{ 1, 0 \right\}$.
To construct a classification rule, we employ the framework of Bayesian logistic regression, which is introduced in the following paragraph.

The mapping can be characterised by the true positive rate (TPR) and the false positive rate (FPR).
The TPR is a measure of sensitivity.
It is the ratio between the number of patients correctly classified as responders and the total number of patients classified as responders.
The FPR is the probability of falsely classifying an actual non-responder.
Thus the FPR is calculated as the ratio between the number of non-responders wrongly categorised as responders and the total number of actual non-responder.

\subsubsection{Bayesian logistic regression}

Bayesian logistic regression is based on data consisting of independent input variables, $\mathbf{ x}^\text{n}$, and dependent binary output variables, $ r^\text{n} \in \left\{ 1, 0 \right\}$.

It is assumed that the dependent outcome variable is Bernoulli distributed, such that
\begin{align} \label{align:log odds function}
p( r^\text{n}) &= \left( p^\text{n}\right)^{r^\text{n}} \left( 1- p^\text{n}\right)^{1-r^\text{n}} \, \text{.}
\end{align}
For the response variable, $p^\text{n}$, one assumes a linear relationship between the input variables and the log-odds of $p^\text{n}$,
\begin{align}
\ln \left( \frac{p^\text{n}}{1-p^\text{n}} \right) &= α + \sum_\text{i} β_\text{i} x^\text{n}_\text{i} \, \text{.}
\end{align}
To solve the inverse problem, to get an unbiased estimate for the model parameters $α$ and $β_\text{i}$, we employ statistical inference.

As a prior for the model parameters, we assume a normal distribution centred at zero with variance $σ^2$.
We employ an inverse gamma distribution as a conjugate prior for the variance of the normal distribution as explained in subsection \ref{subsection: Inverse problems and statistical inference}.
The posterior distribution of the model parameters is determined by the stochastic model
\begin{align}
\begin{split}
σ^2 & \sim \text{InverseGamma}( 2, 3) \\
α & \sim \text{Normal}( 0, σ^2) \\
β_\text{i} & \sim \text{Normal}( 0, σ^2) \\
r^\text{n} & \sim \text{Bernoulli} \left( p^\text{n}\right) \text{ with } 		p^\text{n} \stackrel{ \eqref{align:log odds function}}{=} \frac{1}{ 1 + \exp \left( -α - \sum_\text{i} β_\text{i} x^\text{n}_\text{i}\right)} \, \text{.}
\end{split}
\end{align}
Based on the data, we obtain a maximum a posteriori probability (MAP) estimate for the model parameter, $α^\star$ and $β^\star_\text{i}$.
To this end, we draw a sequence of random samples with a Hamiltonian Monte Carlo algorithm \cite{Betancourt2017} implemented within the probabilistic programming library Turing.jl \cite{Ge2018}.
The sample sequence converges to be distributed according to the posterior distribution.\\

To solve the forward problem, the classification of a patient based on mutational information, we define a score function
\begin{align} \label{align:score function}
s^\text{n}( \mathbf{ x}^\text{n}) = α^\star + \sum_\text{i} β^\star_\text{i} x^\text{n}_\text{i} \, \text{.}
\end{align}
Within this score function we use our maximum posterior estimate of the model parameter.
Our response classification, $\mathbf{ x}^\text{n} \mapsto k^\text{n}$ with
\begin{align} \label{align:response prediction}
k^\text{n} = \begin{cases}
1, & \text{if $ s^\text{n}( \mathbf{ x}^\text{n}) \geq δ$}\\
0, & \text{else}
\end{cases} \, \text{,}
\end{align}
depends on the score function, $s^\text{n}( \mathbf{ x}^\text{n})$, and a variable threshold parameter, $δ$.

\subsubsection{Receiver operating characteristic}

The receiver operating characteristic (ROC) is a method to quantify the performance of a threshold dependent binary classifier.
The ROC was developed in the context of signal detection and gained application in various quantitative research fields.
Nowadays, it is frequently used in medical decision making and as a performance measure in machine learning.
Within this chapter, we employ the ROC curve as a graphical representation of the performance of a classifier.
The area under this curve is a quantitative measure for classification precision.
A rigorous analysis of the ROC can be found in the literature \cite{Fawcett2005}.

The FPR and the TPR, introduced with the binary classification problem, span the two-dimensional ROC space.
Both rates characterise a binary classifier and depend on the threshold of the classifier, $δ$.
One generates the ROC curve by plotting the TPR against FPR for each value of $δ$.
Figure \ref{figure: ROC_Curve_Example} shows the ROC curves of four classifiers with different predictive power.

To quantify the quality of a binary classifier, one can employ the area under the curve (AUC).
One assumes that the value of the score function $s$ is distributed according to the probability density $f_\text{R}( s)$ for patients with clinical response and according to $f_\text{N}( s)$ respectively for patients without response.
Under this hypothesis, TPR and FPR can be represented as an integral,
\begin{align} \label{align:fpr tpr integral equations}
\begin{split}
\text{TPR}( δ) &= \int_δ^\infty f_\text{R}( s) \text{d}s \\
\text{FPR}( δ) &= \int_δ^\infty f_\text{N}( s) \text{d}s \, \text{.}
\end{split}
\end{align}
For a randomly chosen responder, $\mathbf{ x}^\text{n}$, and a randomly chosen non-responder, $\mathbf{ x}^\text{m}$, the AUC is equal to the probability that the inequality $ s^{\text{n}} > s^{\text{m}}$ holds,
\begin{align}
\begin{split}
\text{AUC} & = \int_0^1 \text{TPR} \left( \text{FPR}^{-1}(x)\right) \text{d}x \\
& \stackrel{ \mathclap{ \text{FPR}^{-1}(x)=:δ}}{=} \, \,  -\int_{-\infty}^{\infty} \text{TPR}(δ) \frac{ \partial \,\text{FPR}}{ \partial \, δ}(δ) \, \text{d}δ \\
& \stackrel{ \mathclap{ \eqref{align:fpr tpr integral equations}}}{=} \, \int_{-\infty}^{\infty} \int_{-\infty}^{\infty} f_\text{R}(s) f_\text{N}(δ) θ( s - δ) \, \text{d}s \, \text{d}δ = p( s^{\text{n}} > s^{\text{m}}) \, \text{.}
\end{split}
\end{align}

\begin{figure}
	\centering
	\captionabove[]{Examples of ROC curves for different predictive classifier. Random classifier \tikz \definecolor{MyBlue}{rgb}{0.266122,0.486664,0.802529} \draw[black,fill=MyBlue] (0,0) circle (0.7ex);, weak classifier \tikz \definecolor{MyGreen}{rgb}{0.513417,0.72992,0.440682} \draw[black,fill=MyGreen] (0,0) circle (0.7ex);, strong classifier \tikz \definecolor{MyOrange}{rgb}{0.863512,0.670771,0.236564} \draw[black,fill=MyOrange] (0,0) circle (0.7ex);, perfect classifier \tikz \definecolor{MyRed}{rgb}{0.857359,0.131106,0.132128} \draw[black,fill=MyRed] (0,0) circle (0.7ex);.}
	\includegraphics{chapter/response_prediction/graphics/ROC_Curve_Example-cropped.pdf}
	\label{figure: ROC_Curve_Example}
\end{figure}

\subsection{Survival analysis} \label{subsection: Survival analysis}

Survival analysis is a branch of statistics to analyse the timespan until one event occurs, such as a death in CBI trials.
At the end of observation time, either an event occurred, or the patient was censored.
Censoring takes place if no event is observed and nothing is known about the patient after observation.
Within the context of our CBI response prediction, the observation time starts with treatment.
Censoring occurs when the patient leaves the clinical study on account of any reason without observing an event.
An event may or may not occur after censoring.

We define common terms in survival analysis and introduce the Kaplan-Meier estimator to obtain patient groups' survival rates.
In the closing paragraph, we outline the method of Bayesian linear regression, which we employ for our survival time prediction.

\subsubsection{Common terms in survival analysis}

We refer to survival data as a dataset consisting of observation times, $ t^\text{n} \in \mathbb{R}^{+}$, and information about censoring, $ c^\text{n} \in \{ 1, 0 \}$, where $c^\text{n} = 1$ marks patients leaving the clinical study.
The survival function,
\begin{align}
s^\text{n}(t) = P( t^\text{n} > t) \, \text{,}
\end{align}
gives the probability that a patient will survive beyond time $t$.

To estimate the survival function of a patient group, we employ a Kaplan-Meier estimator.
The Kaplan-Meier estimator of survival rates
\begin{align}
\hat s( t) = \prod_{ \text{i}:t_\text{i} < t} ( 1 - \frac{d_\text{i}}{ n_\text{i}})
\end{align}
depends on $d_{i}$, the number of deaths that happened at $t_{i}$, and $n_{i}$, the patients at risk which have not yet died or been censored up to time $t_{i}$. 
By comparing $\hat s( t)$ between patient groups, one gets evidence for differential efficacy of therapy.
The Kaplan-Meier estimator is a widely used method for demonstrating clinical benefit.

The interested reader can find an extensive mathematical description of survival analysis in the lecture notes \cite{BakryGillMolchanov1992}.

\subsubsection{Bayesian linear regression}

To quantify the relation between mutational information, $\mathbf{ x}^\text{n}$, and survival time, $ t^\text{n}$, we employ the framework of Bayesian linear regression.
On account of unknown event times we have to deal with right-censored data.
Right-censored time spans end later than a certain point in time but it is unknown by how much.
The general assumption of our approach is a linear relation between $x^\text{n}_\text{i}$ and $ t^\text{n}$.
We employ a stochastic model,
\begin{align}
\begin{split}
σ^2 & \sim \text{InverseGamma}( 2, 3) \\
α & \sim \text{Normal}( 0, σ^2) \\
β_\text{i} & \sim \text{Normal}( 0, σ^2) \\
t^\text{n} & \sim  \begin{cases}
\text{Normal}( μ^\text{n}, 1), & \text{if $ c^\text{n} = 0$}\\
\text{CCDFNormal}( μ^\text{n}, 1), & \text{if $ c^\text{n} = 1$}
\end{cases} \, \text{,}
\end{split}
\end{align}
where the inverse-gamma distribution is used as a prior for the variance analogously to our stochastic model of logistic regression.
We assume the uncensored survival times to be distributed according to a normal distribution with mean
\begin{align} \label{eqn: survival time prediction}
μ^\text{n}( \mathbf{ x}^\text{n}) = α + \sum_\text{i} β_\text{i} x^\text{n}_\text{i}
\end{align} 
and unit variance.
We employ the complementary cumulative distribution function (CCDF) of the normal distribution for the right-censored data.
We generate samples with a Hamiltonian Monte Carlo algorithm using the probabilistic programming library Turing.jl \cite{Ge2018}.
If we do not fix the variance, our stochastic model is more unstable, and the Hamilton Monte Carlo algorithm does not converge.
For survival time prediction we employ equation \eqref{eqn: survival time prediction} with a MAP estimate for the model parameter, $ α^\star \text{ and } β_\text{i}^\star$.
