
\section{Hypothetical frameshift-based response determinants} \label{section: Hypothetical frameshift-based response determinants}

In our study, we consider that a high number of frameshift mutations is not the whole story.
Their expression is a precondition for immune recognition.
Frameshift derived peptides must be trimmed for MHC presentation and bind to an MHC molecule.
Finally, the immune system must recognise the peptides as non-self for a frameshift mutation to be immunogenic.
In addition to the number of frameshift mutations, we focus on additional hypothetical frameshift-related determinants and, in particular, on frame shift-derived peptides.
We consider observables without free parameters and search for a clear response signal.

We introduce the safeguard mechanisms nonsense-mediated decay (NMD) and non-stop decay (NSD) that prevent the translation of erroneous mRNA in subsection \ref{subsection: Aspects of genetic mutation within cancer}.
NMD prevents the translation of frameshift sequences that contain a stop codon 50 nucleotides before an exon-exon junction.
NSD safeguards against frameshift-derived mRNA sequences that do not contain a proper stop codon.
Taking into account these safeguard mechanisms, we consider the number of frameshift-derived peptides that are not affected by NMD and NSD.

We expect the clonal structure of the tumour to be relevant for CBI response.
A clonal mutation is likely to cause a more significant immune response than a non-clonal mutation.
In the case of an immune reaction due to a non-clonal mutation, some tumour clones are resistant to the immune reaction.
A study within a mouse model indicates that neoantigens with a low cancer cell fraction (CCF) do not lead to immune-mediated cell rejection and that the CCF threshold for an immune response is antigen-dependent \cite{Gejman2018}.
We assume that the relative measure of a CCF is relevant for immune response.
We define the accumulated frequency of frameshift mutations,
\begin{equation}
f_\text{i} = \sum_{ \text{m} \in \text{FM}_\text{i}} \text{CCF}_\text{m}\, \text{,}
\end{equation}
where $\text{FM}_\text{i}$ is the set of frameshift mutations within the tumour genome of individual i and $\text{CCF}_\text{m}$ is the CCF of mutation m.

The expression of a frameshift mutation is a candidate for a CBI response determinant.
We investigate the accumulated expression of frameshift mutations,
\begin{equation}
e_\text{i} = \sum_{ \text{m} \in \text{FM}_\text{i}} \text{GE}_\text{m} \, \text{,}
\end{equation}
where $\text{GE}_\text{m}$ is the expression of gene m.

MHC presentation relies on trimming proteins into peptides of canonical 8–10 amino acids.
Peptide processing significantly shapes the MHC immunopeptidome, the set of peptides presented on MHC molecules.
However, the specificity of this process is not well understood, and the prediction of peptide processing is not entirely reliable \cite{Lee2020}.
We, therefore, do not consider the specificity of peptide trimming.

We investigate the length of frameshift-derived peptides,
\begin{equation}
l_\text{i} = \sum_{ \text{m} \in \text{FM}_\text{i}} \text{L}_\text{m} \, \text{,}
\end{equation}
where $\text{L}_\text{m}$ denotes the length of a frameshift derived peptide.
In Appendix section \ref{appendix section: Frameshift-derived peptide sequences}, we explain our bioinformatic method for obtaining frameshift-derived peptide sequences.

The presentation of frameshift-derived peptides on MHC molecules on the cell surface is another hypothetical determinant of CBI response.
Therefore we investigate the number of MHC-binding frameshift-derived peptides,
\begin{equation}
b_\text{i} = \sum_{ \text{m} \in \text{FM}_\text{i}} \text{B}_\text{m} \, \text{,}
\end{equation}
where we estimate peptide-MHC (pMHC) binding via NetMHC \cite{Andreatta2016, Nielsen2003}.
We restrict the prediction to peptides with a lenght of 9 amino acids.
Because we do not have information about a patient-specific MHC repertoire, we base our pMHC binding prediction on supertype representatives.
The first set of supertype representatives was defined in 1999 \cite{Sette1999}.
Nowadays, it is possible to cover the binding properties of almost all known MHC molecules based on functional binding specificities of a few supertype representatives \cite{Sidney2008}.

The adaptive immune response depends on an interaction between the T cell receptor (TCR) and the pMHC complex.
An essential feature in the characterisation of the TCR-pMHC interaction is the binding affinity, the electronic property by which the TCR and the pMHC are prone to form a chemical compound.
However, to predict the T cell recognition with the TCR-pMHC affinity is far from conclusive \cite{Galvez2019}.
Maybe the information transmitted to the T Cell, measured by the entropy of TCR-pMHC binding dynamics, is decisive for recognition \cite{Egan2021}.
Thus, we do not consider TCR-pMHC affinity within our study.
