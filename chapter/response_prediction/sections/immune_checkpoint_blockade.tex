
\section{Cancer immunotherapy by immune checkpoint blockade} \label{section:Cancer immunotherapy by immune checkpoint blockade}

An immune checkpoint is a pathway that regulates the immune response.
This regulation is central to immune tolerance, e.g. insensitivity to the microbiome, immune tolerance in pregnancy and tolerance to non-self peptides in food.
During tumour evolution, some tumours acquire the ability to stimulate immune checkpoints that prevent an immune response.
CBI blocks inhibitory checkpoints such as PD-1, PD-L1, and CTLA-4 to restore the protective function of the immune system \cite{Pardoll2012}.

Extensive clinical trials show that within a minority of patients, checkpoint inhibition has outstanding benefits \cite{Ribas2018}.
Although immunotherapy generally has fewer adverse effects than chemotherapy, CBI can cause severe adverse reactions by altering immunologic self-tolerance.
Because only a small patient group receives durable clinical benefits, the high variability in patient response limits the clinical use.
Therefore the understanding of determinants that drive immune response, resistance, and adverse side effects is a key scientific issue in the field of immuno-oncology \cite{Havel2019}.

In the following, we provide an overview of CBI response determinants that have been studied in the literature.
We refer to the expression of tumour suppressor genes, the tumour genome, host germline genetics and the tumour microenvironment.\\

Expression of the immune suppressor gene PD-L1 is a predictive biomarker of CBI response.
Improved efficacy of CBI over chemotherapy, with fewer adverse effects than in chemotherapy, was found in patients with advanced non-small lung cancer (NSCLC) and PD-L1 expression \cite{Reck2016}.
Within another phase III clinical trial, CBI was not associated with significantly longer survival times than chemotherapy among NSCLC patients with PD-L1 expression \cite{Carbone2017}.
Although a metastudy indicates that PD-L1 expression is a predictive biomarker, the PD-L1 expression has limitations, and further determinants have to be carefully investigated \cite{Davis2019}.\\

Tumour genomes contain self-antigens that are a hypothetical determinant for CBI response \cite{Gjerstorff2015}.
These non-mutated antigens are based on tumour overexpressed genes or genes,
only expressed in cells without MHC presentation within healthy tissue.
Thus, self-antigens are potential targets for immune recognition.

Besides non-mutated self-antigens, tumour genomes have antigens based on non-synonymous mutations.
Studies show that there is a correlation between the overall number of mutations and the response to CBI.
This correlation is likely linked to these somatic mutations \cite{Chan2019}.
The overall number of mutations is called tumour mutational burden (TMB).
TMB-based biomarkers are in clinical use.
However, in a pan-cancer dataset of more than 2500 CBI-treated patients, Mirny et al. found little evidence that TMB is a predictive biomarker.
The statistical analysis of this dataset suggests that previously reported correlations are based on confounding cancer subtypes and incorrect statistical testing \cite{Mirny2020}.

Within the tumour genome, microsatellite instability (MSI) is a further response determinant.
MSI is a predisposition to mutations in repetitive DNA sequences due to a non-functioning DNA repair mechanism.
Thus, the MSI consequently leads to a high TMB.
Efficacy of PD-1 blockade therapy is found in cancer patients with MSI across 12 different tumour types \cite{Le2017}.

Frameshift mutation derived peptides are highly distinct from self-peptides.
Thus frameshift mutations in the tumour genome are hypothetically a rich source of immunogenic antigens. 
Across three melanoma studies, Swanton et al. found that the number of frameshift mutations is significantly associated with response to CBI \cite{Swanton2017}.\\

The MHC molecules are encoded by the human leukocyte antigen (HLA) complex.
The HLA system is the most heterogeneous gene complex within the human genome. 
MHC diversity is a critical component for immune defence.
Therefore, a more diverse set of MHC molecules is a hypothetical response determinant.
An analysis of more than 1500 CBI-treated patients gives evidence that a more diverse set of MHC molecules within a patients genome is related with clinical benefit \cite{Chan2018}.
The ability to present a broader range of peptides via MHC molecules on the cell surface may explain this finding.\\

The expression of inhibitory checkpoints is associated with the density and distribution of CD8$^{+}$ T cells within the tumour microenvironment.
The presence of CD8$^{+}$ T cells located at the tumour margin before starting therapy may determine CBI response in metastatic melanoma \cite{Ribas2014}.

Cancer is a systemic disease that causes far-reaching immune system dysfunction.
Thus, the entire immune system can play a crucial role in the tumour macroenvironment.
Accumulating evidence indicates that CBI drives new immune responses rather than enhancing pre-existing ones \cite{Hiam-Galvez2021}.
The understanding of systematic immunity in cancer may be an essential step for reliable response prediction.

In summary, there are numerous hypothetical determinants of response to CBI.
Despite further clinical studies, we need careful analysis of genomic and systemic characteristics of CBI response.